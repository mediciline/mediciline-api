package org.tmcode.event;

public interface MessageSender {

    void send(Object message);
}
