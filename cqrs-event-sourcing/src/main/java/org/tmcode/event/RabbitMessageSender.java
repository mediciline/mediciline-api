package org.tmcode.event;

import org.springframework.amqp.rabbit.core.RabbitTemplate;

import static org.tmcode.event.QueueConfiguration.EXCHANGE_NAME;

class RabbitMessageSender implements MessageSender {

    private final RabbitTemplate rabbitTemplate;

    RabbitMessageSender(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Override
    public void send(Object message) {

        Class<?> clazz = message.getClass();
        String routingKey = clazz.getName();

        rabbitTemplate.convertAndSend(EXCHANGE_NAME, routingKey, message);
    }
}
