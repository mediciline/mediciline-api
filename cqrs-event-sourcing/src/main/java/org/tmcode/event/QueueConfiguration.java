package org.tmcode.event;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;

import javax.annotation.PostConstruct;
import java.lang.reflect.Method;
import java.util.Arrays;

class QueueConfiguration implements BeanFactoryAware {

    static final String EXCHANGE_NAME = "medi";

    private final ApplicationContext applicationContext;
    private ConfigurableBeanFactory configurableBeanFactory;

    QueueConfiguration(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.configurableBeanFactory = (ConfigurableBeanFactory) beanFactory;
    }

    @PostConstruct
    public void onPostConstruct() {

        DirectExchange exchange = new DirectExchange(EXCHANGE_NAME);
        String exchangeBeanName = "exchange." + exchange.getName();
        configurableBeanFactory.registerSingleton(exchangeBeanName, exchange);

        for (String beanName : applicationContext.getBeanDefinitionNames()) {

            Object bean = configurableBeanFactory.getBean(beanName);

            Class<?> clazz = bean.getClass();
            if (org.springframework.aop.support.AopUtils.isAopProxy(bean)) {

                clazz = org.springframework.aop.support.AopUtils.getTargetClass(bean);
            }

            Method[] methods = clazz
                    .getMethods();

            Arrays.stream(methods)
                    .filter(method -> method.isAnnotationPresent(RabbitListener.class))
                    .forEach(method -> registerQueue(beanName, method, exchange));
        }
    }

    private void registerQueue(String beanName, Method method, DirectExchange exchange) {
        if (method.getParameterCount() != 1) {
            throw new IllegalStateException(String.format("method %s in %s must have only one parameter", method.getName(), beanName));
        }

        String[] queues = method.getAnnotation(RabbitListener.class).queues();
        String routingKey = method.getParameters()[0].getType().getName();

        for (String queueName : queues) {
            if (queueName.endsWith(".retry"))
                continue;
            Queue queue = QueueBuilder.durable(queueName)
                    .deadLetterExchange(EXCHANGE_NAME)
                    .deadLetterRoutingKey(queueName + ".retry")
                    .build();

            Queue queueRetry = QueueBuilder.durable(queueName + ".retry")
                    .deadLetterExchange(EXCHANGE_NAME)
                    .deadLetterRoutingKey(queueName)
                    .build();

            Binding binding = BindingBuilder.bind(queue).to(exchange).with(routingKey);
            Binding bindingToRetry = BindingBuilder.bind(queueRetry).to(exchange).with(queueName + ".retry");
            Binding bindingFromRetry = BindingBuilder.bind(queue).to(exchange).with(queueName);

            configurableBeanFactory.registerSingleton("queue." + queueName, queue);
            configurableBeanFactory.registerSingleton("queue." + queueRetry, queueRetry);
            configurableBeanFactory.registerSingleton("binding." + binding.getExchange() + binding.getDestination(), binding);
            configurableBeanFactory.registerSingleton("binding.to.retry." + bindingToRetry.getExchange() + bindingToRetry.getDestination(), bindingToRetry);
            configurableBeanFactory.registerSingleton("binding.from.retry." + bindingFromRetry.getExchange() + bindingFromRetry.getDestination(), bindingFromRetry);
        }
    }
}
