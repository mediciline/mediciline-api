package org.tmcode.cqrs;

public interface Gate {

	void dispatch(Object command);
}
