package org.tmcode.cqrs;

public interface CommandHandler<C> {

    void handle(C command);
}
