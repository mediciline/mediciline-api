package org.tmcode.cqrs;

import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class CqrsAutoConfiguration {

    @Bean
    HandlersProvider handlersProvider(ConfigurableListableBeanFactory beanFactory) {
        return new SpringHandlersProvider(beanFactory);
    }

    @Bean
    Gate gate(HandlersProvider handlersProvider) {
        RunEnvironment runEnvironment = new RunEnvironment(handlersProvider);
        return new StandardGate(runEnvironment);
    }
}
