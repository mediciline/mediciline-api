package org.tmcode.cqrs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class RunEnvironment {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommandHandler.class);

    RunEnvironment(HandlersProvider handlersProvider) {
        this.handlersProvider = handlersProvider;
    }

    private final HandlersProvider handlersProvider;

    void run(Object command) {

        CommandHandler<Object> handler = handlersProvider.getHandler(command);

        LOGGER.info("handle command by {}", handler.getClass().getName());

        handler.handle(command);
    }

}
