package org.tmcode.cqrs;

interface HandlersProvider {

    CommandHandler<Object> getHandler(Object command);
}
