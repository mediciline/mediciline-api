package org.tmcode.cqrs;

class StandardGate implements Gate {

	private final RunEnvironment runEnvironment;

	StandardGate(RunEnvironment runEnvironment) {
		this.runEnvironment = runEnvironment;
	}

	@Override
	public void dispatch(Object command){

		runEnvironment.run(command);
	}
}
