package org.tmcode.mediciline.infrastructure.repository.drug;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import static java.util.UUID.randomUUID;
import static org.assertj.core.api.Assertions.assertThat;

class DrugStatusTest {

    @Test
    public void shouldCreateDrugStatusWithMetaData() {

    	// given
        UUID drugId = UUID.fromString("612cb753-5acb-4a30-8bd1-74c60af37d34");
        UUID dictionaryDrugId = UUID.fromString("a1da065c-c2a4-4dbb-be02-cec817798bf8");
        String expirationDate = "2020-01";
        LocalDateTime eventCreatedAt = LocalDateTime.parse("2011-12-03 10:15:30.123", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));

    	// when
        DrugStatus drugStatus = new DrugStatus(drugId, dictionaryDrugId, expirationDate, eventCreatedAt);

        // then
        assertThat(drugStatus).extracting("metaData").isEqualTo("{\"drugId\":[2011,12,3,10,15,30,123000000],\"dictionaryDrugId\":[2011,12,3,10,15,30,123000000],\"expirationDate\":[2011,12,3,10,15,30,123000000]}");
    }

    @Test
    public void shouldUpdateDrugStatusWithMetaData() {

        // given
        UUID drugId = randomUUID();
        UUID dictionaryDrugId = randomUUID();
        String expirationDate = "2020-01";
        LocalDateTime eventCreatedAt = LocalDateTime.now();
        DrugStatus drugStatus = new DrugStatus(drugId, dictionaryDrugId, expirationDate, eventCreatedAt);

        // when
        drugStatus.setExpirationDate("2020-02", eventCreatedAt.plusDays(1));

        // then
        assertThat(drugStatus).extracting("expirationDate").isEqualTo("2020-02");
    }

    @Test
    public void shouldNotUpdateDrugStatusWithMetaDataWhenNewestEventWasPreviously() {

        // given
        UUID drugId = randomUUID();
        UUID dictionaryDrugId = randomUUID();
        String expirationDate = "2020-01";
        LocalDateTime eventCreatedAt = LocalDateTime.now();
        DrugStatus drugStatus = new DrugStatus(drugId, dictionaryDrugId, expirationDate, eventCreatedAt);

        // when
        drugStatus.setExpirationDate("2020-02", eventCreatedAt.minusSeconds(11));

        // then
        assertThat(drugStatus).extracting("expirationDate").isEqualTo("2020-01");
    }

    @Test
    public void shouldSetExpireDrugStatusWithMetaData() {

        // given
        UUID drugId = randomUUID();
        UUID dictionaryDrugId = randomUUID();
        String expirationDate = "2020-01";
        LocalDateTime eventCreatedAt = LocalDateTime.now();
        DrugStatus drugStatus = new DrugStatus(drugId, dictionaryDrugId, expirationDate, eventCreatedAt);

        // when
        drugStatus.setExpired(true, eventCreatedAt.plusDays(1));

        // then
        assertThat(drugStatus).extracting("expired").isEqualTo(true);
    }

    @Test
    public void shouldNotSetExpireDrugStatusWithMetaDataWhenNewestEventWasPreviously() {

        // given
        UUID drugId = randomUUID();
        UUID dictionaryDrugId = randomUUID();
        String expirationDate = "2020-01";
        LocalDateTime eventCreatedAt = LocalDateTime.now();
        DrugStatus drugStatus = new DrugStatus(drugId, dictionaryDrugId, expirationDate, eventCreatedAt);

        // when
        drugStatus.setExpired(true, eventCreatedAt.minusSeconds(11));

        // then
        assertThat(drugStatus).extracting("expired").isEqualTo(false);
    }
}
