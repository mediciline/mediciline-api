package org.tmcode.mediciline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QueryStartApplication {

    public static void main(String[] args) {
        SpringApplication.run(QueryStartApplication.class, args);
    }
}
