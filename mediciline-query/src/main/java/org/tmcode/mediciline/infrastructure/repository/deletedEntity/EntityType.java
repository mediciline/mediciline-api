package org.tmcode.mediciline.infrastructure.repository.deletedEntity;

enum EntityType {
    DRUG, MEMBER, DISEASE
}
