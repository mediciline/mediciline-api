package org.tmcode.mediciline.infrastructure.repository.disease;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.tmcode.mediciline.dto.DiseaseDto;
import org.tmcode.mediciline.application.query.disease.DiseaseFinder;
import org.tmcode.mediciline.event.DiseaseCreatedEvent;
import org.tmcode.mediciline.event.DiseaseDeletedEvent;
import org.tmcode.mediciline.event.DiseaseFinishedEvent;
import org.tmcode.mediciline.event.DiseaseUpdatedEvent;
import org.tmcode.mediciline.event.MemberDeletedEvent;
import org.tmcode.mediciline.infrastructure.repository.deletedEntity.DeletedEntityRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

import static java.util.stream.Collectors.toUnmodifiableList;
import static org.tmcode.mediciline.event.QueuesDefinition.*;

@Slf4j
@Component
class DiseaseStatusProjection implements DiseaseFinder {

    private final static DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private final DiseaseStatusRepository diseaseStatusRepository;
    private final EntityManager entityManager;
    private final DiseaseDtoFactory diseaseDtoFactory;
    private final DeletedEntityRepository deletedEntityRepository;

    @Autowired
    DiseaseStatusProjection(DiseaseStatusRepository diseaseStatusRepository, EntityManager entityManager, DiseaseDtoFactory diseaseDtoFactory, DeletedEntityRepository deletedEntityRepository) {
        this.diseaseStatusRepository = diseaseStatusRepository;
        this.entityManager = entityManager;
        this.diseaseDtoFactory = diseaseDtoFactory;
        this.deletedEntityRepository = deletedEntityRepository;
    }

    @Transactional
    @RabbitListener(queues = {DISEASE_CREATED_EVENT_DISEASE_STATUS_PROJECTION, DISEASE_CREATED_EVENT_DISEASE_STATUS_PROJECTION_RETRY})
    public void on(DiseaseCreatedEvent event) {
        DiseaseStatus diseaseStatus = new DiseaseStatus(
                event.getDiseaseId(),
                event.getMemberId(),
                event.getName(),
                event.getDescription(),
                event.getStartDate().format(FORMATTER),
                event.getCreatedAt());
        diseaseStatusRepository.save(diseaseStatus);
    }

    @Transactional
    @RabbitListener(queues = {DISEASE_DELETED_EVENT_DISEASE_STATUS_PROJECTION, DISEASE_DELETED_EVENT_DISEASE_STATUS_PROJECTION_RETRY})
    public void on(DiseaseDeletedEvent event) {
        findAndConsume(event.getMemberId(), event.getDiseaseId(), diseaseStatusRepository::delete);
    }

    @Transactional
    @RabbitListener(queues = {DISEASE_UPDATED_EVENT_DISEASE_STATUS_PROJECTION, DISEASE_UPDATED_EVENT_DISEASE_STATUS_PROJECTION_RETRY})
    public void on(DiseaseUpdatedEvent event) {
        findAndConsume(event.getMemberId(), event.getDiseaseId(), diseaseStatus -> update(diseaseStatus, event));
    }

    private void update(DiseaseStatus diseaseStatus, DiseaseUpdatedEvent event) {
        diseaseStatus.setName(event.getName(), event.getCreatedAt());
        diseaseStatus.setDescription(event.getDescription(), event.getCreatedAt());
        diseaseStatus.setStartDate(event.getStartDate().format(FORMATTER), event.getCreatedAt());
        diseaseStatusRepository.save(diseaseStatus);
    }

    @Transactional
    @RabbitListener(queues = {DISEASE_FINISHED_EVENT_DISEASE_STATUS_PROJECTION, DISEASE_FINISHED_EVENT_DISEASE_STATUS_PROJECTION_RETRY})
    public void on(DiseaseFinishedEvent event) {
        findAndConsume(event.getMemberId(), event.getDiseaseId(), diseaseStatus -> finish(diseaseStatus, event));
    }
    @Transactional
    @RabbitListener(queues = {MEMBER_DELETED_EVENT_DISEASE_STATUS_PROJECTION, MEMBER_DELETED_EVENT_DISEASE_STATUS_PROJECTION_RETRY})
    public void on(MemberDeletedEvent event) {
        listDiseaseStatusesByMember(event.getMemberId()).forEach(this::delete);
    }

    private void delete(DiseaseStatus diseaseStatus) {
        findAndConsume(diseaseStatus.getMemberId(), diseaseStatus.getId(), diseaseStatusRepository::delete);
        deletedEntityRepository.addDeletedDisease(diseaseStatus.getId());
    }

    private void finish(DiseaseStatus diseaseStatus, DiseaseFinishedEvent event) {
        diseaseStatus.setEndDate(event.getFinishDate().format(FORMATTER), event.getCreatedAt());
        diseaseStatusRepository.save(diseaseStatus);
    }

    private void findAndConsume(UUID memberId, UUID diseaseId, Consumer<DiseaseStatus> consumer) {
        diseaseStatusRepository.findByMemberIdAndDiseaseId(memberId, diseaseId).ifPresentOrElse(consumer, () -> {
            log.warn("Can not find diseaseStatus with id {}", diseaseId);
            if (deletedEntityRepository.diseaseWasNotDeleted(diseaseId)) {
                throw new EntityNotFoundException();
            }
        });
    }

    @Override
    public List<DiseaseDto> listAllDiseasesByMember(UUID memberId) {
        List<DiseaseStatus> diseaseStatuses = listDiseaseStatusesByMember(memberId);

        return diseaseStatuses.stream()
                .map(diseaseDtoFactory::create)
                .collect(toUnmodifiableList());
    }

    @SuppressWarnings("unchecked")
    private List<DiseaseStatus> listDiseaseStatusesByMember(UUID memberId) {
        return entityManager.createQuery("select d from DiseaseStatus d where d.memberId = :memberId")
                    .setParameter("memberId", memberId)
                    .getResultList();
    }

    @Override
    public DiseaseDto findDisease(UUID diseaseId) {
        DiseaseStatus diseaseStatus = (DiseaseStatus) entityManager.createQuery("select d from DiseaseStatus d where d.id = :diseaseId")
                .setParameter("diseaseId", diseaseId)
                .getSingleResult();

        return diseaseDtoFactory.create(diseaseStatus);
    }
}
