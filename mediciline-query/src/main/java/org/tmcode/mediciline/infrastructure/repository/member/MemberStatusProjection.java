package org.tmcode.mediciline.infrastructure.repository.member;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.tmcode.mediciline.dto.MemberDto;
import org.tmcode.mediciline.application.query.member.MemberFinder;
import org.tmcode.mediciline.event.MemberCreatedEvent;
import org.tmcode.mediciline.event.MemberDeletedEvent;
import org.tmcode.mediciline.event.MemberUpdatedEvent;
import org.tmcode.mediciline.infrastructure.repository.deletedEntity.DeletedEntityRepository;

import javax.persistence.EntityNotFoundException;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

import static java.util.stream.Collectors.toUnmodifiableList;
import static org.tmcode.mediciline.event.QueuesDefinition.*;

@Slf4j
@Component
class MemberStatusProjection implements MemberFinder {

    private final static DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private final MemberStatusRepository memberStatusRepository;
    private final MemberDtoFactory memberDtoFactory;
    private final DeletedEntityRepository deletedEntityRepository;

    @Autowired
    MemberStatusProjection(MemberStatusRepository memberStatusRepository, MemberDtoFactory memberDtoFactory, DeletedEntityRepository deletedEntityRepository) {
        this.memberStatusRepository = memberStatusRepository;
        this.memberDtoFactory = memberDtoFactory;
        this.deletedEntityRepository = deletedEntityRepository;
    }

    @Transactional
    @RabbitListener(queues = {MEMBER_CREATED_EVENT_MEMBER_STATUS_PROJECTION, MEMBER_CREATED_EVENT_MEMBER_STATUS_PROJECTION_RETRY})
    public void on(MemberCreatedEvent event) {
        memberStatusRepository.save(new MemberStatus(event.getMemberId(), event.getName(), event.getBirthday().format(FORMATTER), event.getCreatedAt()));
    }

    @Transactional
    @RabbitListener(queues = {MEMBER_UPDATED_EVENT_MEMBER_STATUS_PROJECTION, MEMBER_UPDATED_EVENT_MEMBER_STATUS_PROJECTION_RETRY})
    public void on(MemberUpdatedEvent event) {
        findAndConsume(event.getMemberId(), memberStatus -> update(memberStatus, event));
    }

    private void update(MemberStatus memberStatus, MemberUpdatedEvent event) {
        String birthday = event.getBirthday().format(FORMATTER);
        memberStatus.setBirthday(birthday, event.getCreatedAt());
        memberStatus.setName(event.getName(), event.getCreatedAt());
        memberStatusRepository.save(memberStatus);
    }

    @Transactional
    @RabbitListener(queues = {MEMBER_DELETED_EVENT_MEMBER_STATUS_PROJECTION, MEMBER_DELETED_EVENT_MEMBER_STATUS_PROJECTION_RETRY})
    public void on(MemberDeletedEvent event) {
        findAndConsume(event.getMemberId(), memberStatusRepository::delete);
        deletedEntityRepository.addDeletedMember(event.getMemberId());
    }

    private void findAndConsume(UUID memberId, Consumer<MemberStatus> consumer) {
        memberStatusRepository.findByMemberId(memberId).ifPresentOrElse(consumer, () -> {
            log.warn("Can not find memberStatus with id {}", memberId);
            if (deletedEntityRepository.memberWasNotDeleted(memberId)) {
                throw new EntityNotFoundException();
            }
        });
    }

    @Override
    public List<MemberDto> listAllMembers() {
        return memberStatusRepository.findAll().stream()
                .map(memberDtoFactory::create)
                .collect(toUnmodifiableList());
    }

    @Override
    public MemberDto getMember(UUID memberId) {
        return memberStatusRepository.findByMemberId(memberId)
                .map(memberDtoFactory::create)
                .orElseThrow(EntityNotFoundException::new);
    }
}
