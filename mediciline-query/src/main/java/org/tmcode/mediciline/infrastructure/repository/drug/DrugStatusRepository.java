package org.tmcode.mediciline.infrastructure.repository.drug;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
interface DrugStatusRepository extends JpaRepository<DrugStatus, UUID> {

    @Query("select d from DrugStatus d where d.id = ?1")
    Optional<DrugStatus> findByDrugId(UUID aggregateId);
}
