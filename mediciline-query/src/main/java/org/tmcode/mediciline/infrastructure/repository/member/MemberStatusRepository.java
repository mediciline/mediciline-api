package org.tmcode.mediciline.infrastructure.repository.member;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
interface MemberStatusRepository extends JpaRepository<MemberStatus, UUID> {

    @Query("select m from MemberStatus m where m.id = ?1")
    Optional<MemberStatus> findByMemberId(UUID memberId);
}
