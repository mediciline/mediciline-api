package org.tmcode.mediciline.infrastructure.repository.member;

import org.springframework.stereotype.Component;
import org.tmcode.mediciline.dto.MemberDto;

@Component
class MemberDtoFactory {
    MemberDto create(MemberStatus memberStatus) {
        return new MemberDto(memberStatus.getId(), memberStatus.getName(), memberStatus.getBirthday());
    }
}
