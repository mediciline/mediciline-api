package org.tmcode.mediciline.infrastructure.repository.drug;

import org.springframework.stereotype.Component;
import org.tmcode.mediciline.dto.DrugDto;

@Component
class DrugDtoFactory {
    DrugDto create(DrugStatus drugStatus) {
        return new DrugDto(drugStatus.getId(), drugStatus.getDictionaryDrugId(), drugStatus.getExpirationDate(), drugStatus.isExpired());
    }
}
