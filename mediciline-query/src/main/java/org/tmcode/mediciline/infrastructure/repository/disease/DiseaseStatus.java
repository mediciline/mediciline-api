package org.tmcode.mediciline.infrastructure.repository.disease;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.tmcode.mediciline.infrastructure.repository.MetaDataFactory;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Getter
@NoArgsConstructor
class DiseaseStatus {

    @Id
    @Type(type = "uuid-char")
    private UUID id;
    @Type(type = "uuid-char")
    private UUID memberId;
    private String name;
    private String description;
    private String startDate;
    private String endDate;
    @Getter(AccessLevel.NONE)
    private String metaData;

    DiseaseStatus(UUID diseaseId, UUID memberId, String name, String description, String startDate, LocalDateTime eventCreatedAt) {
        this.id = diseaseId;
        this.memberId = memberId;
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.metaData = MetaDataFactory.create(eventCreatedAt, "name", "description", "startDate", "endDate");
    }

    void setName(String name, LocalDateTime eventCreatedAt) {
        LocalDateTime previousEventCreatedAt = MetaDataFactory.read(metaData, "name");
        if (previousEventCreatedAt.isBefore(eventCreatedAt)) {
            this.name = name;
        }
    }

    void setDescription(String description, LocalDateTime eventCreatedAt) {
        LocalDateTime previousEventCreatedAt = MetaDataFactory.read(metaData, "description");
        if (previousEventCreatedAt.isBefore(eventCreatedAt)) {
            this.description = description;
        }
    }

    void setStartDate(String startDate, LocalDateTime eventCreatedAt) {
        LocalDateTime previousEventCreatedAt = MetaDataFactory.read(metaData, "startDate");
        if (previousEventCreatedAt.isBefore(eventCreatedAt)) {
            this.startDate = startDate;
        }
    }

    void setEndDate(String endDate, LocalDateTime eventCreatedAt) {
        LocalDateTime previousEventCreatedAt = MetaDataFactory.read(metaData, "endDate");
        if (previousEventCreatedAt.isBefore(eventCreatedAt)) {
            this.endDate = endDate;
        }
    }
}
