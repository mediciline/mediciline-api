package org.tmcode.mediciline.infrastructure.repository.disease;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
interface DiseaseStatusRepository extends JpaRepository<DiseaseStatus, UUID> {

    @Query("select d from DiseaseStatus d where d.memberId = ?1 and d.id = ?2")
    Optional<DiseaseStatus> findByMemberIdAndDiseaseId(UUID memberId, UUID diseaseId);
}
