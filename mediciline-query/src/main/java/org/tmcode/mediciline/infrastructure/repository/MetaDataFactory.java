package org.tmcode.mediciline.infrastructure.repository;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.common.collect.Maps;
import lombok.SneakyThrows;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public abstract class MetaDataFactory {

    private static final ObjectMapper objectMapper = new ObjectMapper()
            .setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY)
            .registerModule(new JavaTimeModule());

    @SneakyThrows
    public static String create(LocalDateTime eventCreatedAt, String... fields) {
        HashMap<String, LocalDateTime> map = Maps.newHashMap();
        Arrays.stream(fields).forEach(o -> map.put(o, eventCreatedAt));
        return objectMapper.writer().writeValueAsString(map);
    }

    @SneakyThrows
    public static LocalDateTime read(String metaData, String field) {
        TypeReference<HashMap<String, LocalDateTime>> typeRef = new TypeReference<>() {};

        Map<String, LocalDateTime> stringObjectMap = objectMapper.readValue(metaData, typeRef);
        return stringObjectMap.get(field);
    }
}
