package org.tmcode.mediciline.infrastructure.repository.disease;

import org.springframework.stereotype.Component;
import org.tmcode.mediciline.dto.DiseaseDto;

@Component
class DiseaseDtoFactory {
    DiseaseDto create(DiseaseStatus diseaseStatus) {
        return new DiseaseDto(
                diseaseStatus.getId(),
                diseaseStatus.getMemberId(),
                diseaseStatus.getName(),
                diseaseStatus.getDescription(),
                diseaseStatus.getStartDate(),
                diseaseStatus.getEndDate()
        );
    }
}
