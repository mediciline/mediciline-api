package org.tmcode.mediciline.infrastructure.repository.deletedEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static java.util.UUID.randomUUID;
import static org.springframework.transaction.annotation.Propagation.REQUIRES_NEW;
import static org.tmcode.mediciline.infrastructure.repository.deletedEntity.EntityType.*;

@Repository
public interface DeletedEntityRepository extends JpaRepository<DeletedEntity, UUID> {

    @Query("select case when (count(e) = 0) then true else false end from DeletedEntity e where e.entityId = ?1 and e.type = 'DRUG'")
    boolean drugWasNotDeleted(UUID entityId);

    @Transactional(propagation = REQUIRES_NEW)
    default void addDeletedDrug(UUID drugId) {
        save(new DeletedEntity(randomUUID(), drugId, DRUG));
    }

    @Query("select case when (count(e) = 0) then true else false end from DeletedEntity e where e.entityId = ?1 and e.type = 'MEMBER'")
    boolean memberWasNotDeleted(UUID memberId);

    @Transactional(propagation = REQUIRES_NEW)
    default void addDeletedMember(UUID memberId) {
        save(new DeletedEntity(randomUUID(), memberId, MEMBER));
    }
    @Query("select case when (count(e) = 0) then true else false end from DeletedEntity e where e.entityId = ?1 and e.type = 'DISEASE'")
    boolean diseaseWasNotDeleted(UUID diseaseId);

    @Transactional(propagation = REQUIRES_NEW)
    default void addDeletedDisease(UUID diseaseId) {
        save(new DeletedEntity(randomUUID(), diseaseId, DISEASE));
    }
}
