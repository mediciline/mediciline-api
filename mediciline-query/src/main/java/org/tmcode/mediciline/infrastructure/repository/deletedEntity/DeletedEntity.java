package org.tmcode.mediciline.infrastructure.repository.deletedEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Entity
class DeletedEntity {

    @Id
    @Type(type = "uuid-char")
    private UUID id;
    @Type(type = "uuid-char")
    private UUID entityId;
    @Enumerated(EnumType.STRING)
    private EntityType type;
}

