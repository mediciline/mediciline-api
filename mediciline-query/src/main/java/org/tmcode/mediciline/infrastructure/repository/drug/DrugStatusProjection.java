package org.tmcode.mediciline.infrastructure.repository.drug;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.tmcode.mediciline.application.query.drug.DrugFinder;
import org.tmcode.mediciline.dto.DrugDto;
import org.tmcode.mediciline.event.DrugCreatedEvent;
import org.tmcode.mediciline.event.DrugDeletedEvent;
import org.tmcode.mediciline.event.DrugExpiredEvent;
import org.tmcode.mediciline.event.DrugNotExpiredEvent;
import org.tmcode.mediciline.event.DrugUpdatedEvent;
import org.tmcode.mediciline.infrastructure.repository.deletedEntity.DeletedEntityRepository;
import org.tmcode.mediciline.vo.ExpirationDate;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

import static java.util.stream.Collectors.toUnmodifiableList;
import static org.tmcode.mediciline.event.QueuesDefinition.*;

@Component
@Slf4j
class DrugStatusProjection implements DrugFinder {

    private final DrugStatusRepository drugStatusRepository;
    private final EntityManager entityManager;
    private final DrugDtoFactory drugDtoFactory;
    private final DeletedEntityRepository deletedEntityRepository;

    @Autowired
    DrugStatusProjection(DrugStatusRepository drugStatusRepository, EntityManager entityManager, DrugDtoFactory drugDtoFactory, DeletedEntityRepository deletedEntityRepository) {
        this.drugStatusRepository = drugStatusRepository;
        this.entityManager = entityManager;
        this.drugDtoFactory = drugDtoFactory;
        this.deletedEntityRepository = deletedEntityRepository;
    }

    @Transactional
    @RabbitListener(queues = {DRUG_CREATED_EVENT_DRUG_STATUS_PROJECTION, DRUG_CREATED_EVENT_DRUG_STATUS_PROJECTION_RETRY})
    public void on(DrugCreatedEvent event) {
        ExpirationDate expirationDate = event.getExpirationDate();
        drugStatusRepository.save(new DrugStatus(event.getDrugId(), event.getDictionaryDrugId(), expirationDate.obtainValue(), event.getCreatedAt()));
    }

    @Transactional
    @RabbitListener(queues = {DRUG_UPDATED_EVENT_DRUG_STATUS_PROJECTION, DRUG_UPDATED_EVENT_DRUG_STATUS_PROJECTION_RETRY})
    public void on(DrugUpdatedEvent event) {
        findAndConsume(event.getDrugId(), drugStatus -> update(drugStatus, event));
    }

    private void update(DrugStatus drugStatus, DrugUpdatedEvent event) {
        drugStatus.setExpirationDate(event.getExpirationDate().obtainValue(), event.getCreatedAt());
        drugStatusRepository.save(drugStatus);
    }

    @Transactional
    @RabbitListener(queues = {DRUG_DELETED_EVENT_DRUG_STATUS_PROJECTION, DRUG_DELETED_EVENT_DRUG_STATUS_PROJECTION_RETRY})
    public void on(DrugDeletedEvent event) {
        findAndConsume(event.getDrugId(), drugStatusRepository::delete);
        deletedEntityRepository.addDeletedDrug(event.getDrugId());
    }

    @Transactional
    @RabbitListener(queues = {DRUG_EXPIRED_EVENT_DRUG_STATUS_PROJECTION, DRUG_EXPIRED_EVENT_DRUG_STATUS_PROJECTION_RETRY})
    public void on(DrugExpiredEvent event) {
        findAndConsume(event.getDrugId(), drugStatus -> expire(drugStatus, event));
    }

    private void expire(DrugStatus drugStatus, DrugExpiredEvent event) {
        drugStatus.setExpired(true, event.getCreatedAt());
        drugStatusRepository.save(drugStatus);
    }

    @Transactional
    @RabbitListener(queues = {DRUG_NOT_EXPIRED_EVENT_DRUG_STATUS_PROJECTION, DRUG_NOT_EXPIRED_EVENT_DRUG_STATUS_PROJECTION_RETRY})
    public void on(DrugNotExpiredEvent event) {
        findAndConsume(event.getDrugId(), drugStatus -> notExpire(drugStatus, event));
    }

    private void notExpire(DrugStatus drugStatus, DrugNotExpiredEvent event) {
        drugStatus.setExpired(false, event.getCreatedAt());
        drugStatusRepository.save(drugStatus);
    }

    private void findAndConsume(UUID drugId, Consumer<DrugStatus> consumer) {
        drugStatusRepository.findByDrugId(drugId).ifPresentOrElse(consumer, () -> {
            log.warn("Can not find drugStatus with id {}", drugId);
            if (deletedEntityRepository.drugWasNotDeleted(drugId)) {
                throw new EntityNotFoundException();
            }
        });
    }

    @Override
    public List<DrugDto> findAllDrugs() {
        return drugStatusRepository.findAll().stream()
                .map(drugDtoFactory::create)
                .collect(toUnmodifiableList());
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<UUID> listAllByExpired(boolean expired) {
        return entityManager.createQuery("select d.id from DrugStatus d where d.expired = :expired")
                .setParameter("expired", expired)
                .getResultList();
    }

    @Override
    public DrugDto getById(UUID drugId) {
        return drugStatusRepository.findByDrugId(drugId)
                .map(drugDtoFactory::create)
                .orElseThrow(EntityNotFoundException::new);
    }
}
