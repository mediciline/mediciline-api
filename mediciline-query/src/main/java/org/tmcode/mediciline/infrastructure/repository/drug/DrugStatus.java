package org.tmcode.mediciline.infrastructure.repository.drug;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.tmcode.mediciline.infrastructure.repository.MetaDataFactory;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Getter
@NoArgsConstructor
class DrugStatus {

    @Id
    @Type(type = "uuid-char")
    private UUID id;
    @Type(type = "uuid-char")
    private UUID dictionaryDrugId;
    private String expirationDate;
    private boolean expired;
    @Getter(AccessLevel.NONE)
    private String metaData;

    DrugStatus(UUID drugId, UUID dictionaryDrugId, String expirationDate, LocalDateTime eventCreatedAt) {
        this.id = drugId;
        this.dictionaryDrugId = dictionaryDrugId;
        this.expirationDate = expirationDate;
        this.metaData = MetaDataFactory.create(eventCreatedAt, "expirationDate", "expired");
    }
    void setExpirationDate(String expirationDate, LocalDateTime eventCreatedAt) {
        LocalDateTime previousEventCreatedAt = MetaDataFactory.read(metaData, "expirationDate");
        if (previousEventCreatedAt.isBefore(eventCreatedAt)) {
            this.expirationDate = expirationDate;
        }
    }

    void setExpired(boolean expired, LocalDateTime eventCreatedAt) {
        LocalDateTime previousEventCreatedAt = MetaDataFactory.read(metaData, "expired");
        if (previousEventCreatedAt.isBefore(eventCreatedAt)) {
            this.expired = expired;
        }
    }
}
