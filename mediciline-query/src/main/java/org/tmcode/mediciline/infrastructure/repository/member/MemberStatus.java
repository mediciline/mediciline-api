package org.tmcode.mediciline.infrastructure.repository.member;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.tmcode.mediciline.infrastructure.repository.MetaDataFactory;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Getter
@NoArgsConstructor
class MemberStatus {

    @Id
    @Type(type = "uuid-char")
    private UUID id;
    private String name;
    private String birthday;
    @Getter(AccessLevel.NONE)
    private String metaData;

    MemberStatus(UUID memberId, String name, String birthday, LocalDateTime eventCreatedAt) {
        this.id = memberId;
        this.name = name;
        this.birthday = birthday;
        this.metaData = MetaDataFactory.create(eventCreatedAt, "name", "birthday");
    }

    void setBirthday(String birthday, LocalDateTime eventCreatedAt) {
        LocalDateTime previousEventCreatedAt = MetaDataFactory.read(metaData, "birthday");
        if (previousEventCreatedAt.isBefore(eventCreatedAt)) {
            this.birthday = birthday;
        }
    }

    void setName(String name, LocalDateTime eventCreatedAt) {
        LocalDateTime previousEventCreatedAt = MetaDataFactory.read(metaData, "name");
        if (previousEventCreatedAt.isBefore(eventCreatedAt)) {
            this.name = name;
        }
    }
}
