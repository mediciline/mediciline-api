package org.tmcode.mediciline.application.query.member;

import org.tmcode.mediciline.dto.MemberDto;

import java.util.List;
import java.util.UUID;

public interface MemberFinder {

    List<MemberDto> listAllMembers();

    MemberDto getMember(UUID memberId);
}
