package org.tmcode.mediciline.application.query.drug;

import org.tmcode.mediciline.dto.DrugDto;

import java.util.List;
import java.util.UUID;

public interface DrugFinder {

    List<DrugDto> findAllDrugs();

    List<UUID> listAllByExpired(boolean expired);

    DrugDto getById(UUID drugId);
}
