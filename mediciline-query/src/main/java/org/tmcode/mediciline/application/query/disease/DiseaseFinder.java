package org.tmcode.mediciline.application.query.disease;

import org.tmcode.mediciline.dto.DiseaseDto;

import java.util.List;
import java.util.UUID;

public interface DiseaseFinder {

    List<DiseaseDto> listAllDiseasesByMember(UUID memberId);

    DiseaseDto findDisease(UUID diseaseId);
}
