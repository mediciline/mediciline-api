package org.tmcode.mediciline.interfaces.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.tmcode.mediciline.application.query.drug.DrugFinder;
import org.tmcode.mediciline.dto.DrugDto;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/drugs")
class DrugController {

    private final DrugFinder drugFinder;

    @Autowired
    DrugController(DrugFinder drugFinder) {
        this.drugFinder = drugFinder;
    }

    @GetMapping(path = "{drugId}")
    public DrugDto getById(@PathVariable UUID drugId) {
        return drugFinder.getById(drugId);
    }

    @GetMapping
    public List<DrugDto> findAll() {
        return drugFinder.findAllDrugs();
    }

    @GetMapping(params = "expired")
    public List<UUID> listAllNotExpired(@RequestParam(name = "expired") boolean expired) {
        return drugFinder.listAllByExpired(expired);
    }
}
