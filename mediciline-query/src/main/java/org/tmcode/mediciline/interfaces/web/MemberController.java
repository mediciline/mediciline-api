package org.tmcode.mediciline.interfaces.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tmcode.mediciline.dto.DiseaseDto;
import org.tmcode.mediciline.application.query.disease.DiseaseFinder;
import org.tmcode.mediciline.dto.MemberDto;
import org.tmcode.mediciline.application.query.member.MemberFinder;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/members")
class MemberController {

    private final MemberFinder memberFinder;
    private final DiseaseFinder diseaseFinder;

    @Autowired
    MemberController(MemberFinder memberFinder, DiseaseFinder diseaseFinder) {
        this.memberFinder = memberFinder;
        this.diseaseFinder = diseaseFinder;
    }

    @GetMapping
    public List<MemberDto> listAllMembers() {
        return memberFinder.listAllMembers();
    }

    @GetMapping(path = "{memberId}")
    public MemberDto getMember(@PathVariable UUID memberId) {
        return memberFinder.getMember(memberId);
    }

    @GetMapping(path = "{memberId}/diseases")
    public List<DiseaseDto> listAllDiseaseByMember(@PathVariable UUID memberId) {
        return diseaseFinder.listAllDiseasesByMember(memberId);
    }

    @GetMapping(path = "{memberId}/diseases/{diseaseId}")
    public DiseaseDto listAllDiseaseByMember(@PathVariable UUID memberId, @PathVariable UUID diseaseId) {
        return diseaseFinder.findDisease(diseaseId);
    }
}
