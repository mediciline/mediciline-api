package org.tmcode.mediciline;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
class HttpRequest {

    private final int port;
    private final String path;
    private final String content;
}
