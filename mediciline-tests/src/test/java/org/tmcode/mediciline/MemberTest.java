package org.tmcode.mediciline;

import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.tmcode.mediciline.dto.CreateMemberDto;
import org.tmcode.mediciline.dto.MemberDto;
import org.tmcode.mediciline.dto.UpdateMemberDto;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

class MemberTest {

    private MemberHelper memberHelper;

    @BeforeEach
    public void beforeEach() throws Exception {

        memberHelper = new MemberHelper();
        memberHelper.clearTable();
    }

    @Test
    public void shouldCreateMember() throws Exception {

        // given
        String name = "my name";
        String birthday = "1984-08-09";
        CreateMemberDto createMemberDto = new CreateMemberDto(name, LocalDate.parse(birthday));

        //when
        UUID memberId = memberHelper.createMember(createMemberDto);

        // then
        MemberDto dto = memberHelper.getMember(memberId);
        assertThat(dto.getId()).isEqualTo(memberId);
        assertThat(dto.getBirthday()).isEqualTo(birthday);
        assertThat(dto.getName()).isEqualTo(name);
    }

    @Test
    public void shouldEditMember() throws Exception {

        // given
        String name = "my name";
        String birthday = "1984-08-09";
        CreateMemberDto createMemberDto = new CreateMemberDto(name, LocalDate.parse(birthday));
        UUID memberId = memberHelper.createMember(createMemberDto);

        String newName = "my new name";
        String newBirthday = "1984-10-09";
        UpdateMemberDto updateMemberDto = new UpdateMemberDto(newName, LocalDate.parse(newBirthday));

        //when
        UUID uuid = memberHelper.editMember(updateMemberDto, memberId);

        // then
        MemberDto dto = memberHelper.getMember(memberId);
        assertThat(dto.getId()).isEqualTo(memberId);
        assertThat(dto.getId()).isEqualTo(uuid);
        assertThat(dto.getName()).isEqualTo(newName);
        assertThat(dto.getBirthday()).isEqualTo(newBirthday);
    }

    @Test
    public void shouldDeleteMember() throws Exception {

        // given
        UUID memberId = memberHelper.createMember(new CreateMemberDto("my name", LocalDate.parse("1984-08-09")));

        // when
        memberHelper.deleteMemberId(memberId);

        // then
        Assertions.assertThatThrownBy(() -> memberHelper.getMember(memberId))
                .isInstanceOf(UnrecognizedPropertyException.class);
    }

    @Test
    public void shouldListAllMembers() throws Exception {

        // given
        memberHelper.createMember(new CreateMemberDto("my name 2", LocalDate.parse("1984-05-09")));
        memberHelper.createMember(new CreateMemberDto("my name", LocalDate.parse("1984-08-09")));

        // when
        List<MemberDto> drugDtos = memberHelper.listAllMembers();

        // then
        assertThat(drugDtos).hasSize(2);
    }
}
