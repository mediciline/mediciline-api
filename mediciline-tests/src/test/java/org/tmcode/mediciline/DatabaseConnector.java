package org.tmcode.mediciline;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Objects;

class DatabaseConnector {

    private final String databaseHostname = Objects.requireNonNullElse(System.getProperty("api"), "localhost");

    void execute(String sql) throws Exception{
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection connection = DriverManager.getConnection(
                "jdbc:mysql://" + databaseHostname + ":13306/mediciline?useUnicode=true&characterEncoding=utf-8","mediciline","j8rYn82yVLQkvnFa");
        Statement statement = connection.createStatement();
        statement.execute(sql);
        connection.close();
    }
}
