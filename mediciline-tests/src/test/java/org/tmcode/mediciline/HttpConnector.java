package org.tmcode.mediciline;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

import java.util.Objects;
import java.util.UUID;

class HttpConnector {

    private final String api = Objects.requireNonNullElse(System.getProperty("api"), "localhost");
    private final OkHttpClient httpClient = new OkHttpClient();

    String post(HttpRequest httpRequest) throws Exception {
        Thread.sleep(500);
        RequestBody requestBody = RequestBody.create(httpRequest.getContent(), MediaType.get("application/json"));
        Request request = new Request.Builder()
                .url(String.format("http://%s:%s/%s", api, httpRequest.getPort(), httpRequest.getPath()))
                .post(requestBody)
                .build();
        return httpClient.newCall(request)
                .execute()
                .body()
                .string();
    }

    String put(HttpRequest httpRequest) throws Exception {
            Thread.sleep(500);
            RequestBody requestBody = RequestBody.create(httpRequest.getContent(), MediaType.get("application/json"));
            Request request = new Request.Builder()
                    .url(String.format("http://%s:%s/%s", api, httpRequest.getPort(), httpRequest.getPath()))
                    .put(requestBody)
                    .build();
            return httpClient.newCall(request)
                    .execute()
                    .body()
                    .string();
        }

    String get(HttpRequest httpRequest) throws Exception {
        Thread.sleep(500);
        Request request = new Request.Builder()
                .url(String.format("http://%s:%s/%s", api, httpRequest.getPort(), httpRequest.getPath()))
                .get()
                .build();
        return httpClient.newCall(request)
                .execute()
                .body()
                .string();
    }


    void patch(UUID id) throws Exception {
        Thread.sleep(500);
        RequestBody requestBody = RequestBody.create("", MediaType.get("application/json"));
        Request request = new Request.Builder()
                .url(String.format("http://%s:8081/api/drugs/%s", api, id))
                .patch(requestBody)
                .build();
        httpClient.newCall(request)
                .execute();
    }

    String patch(HttpRequest httpRequest) throws Exception {
        Thread.sleep(500);
        RequestBody requestBody = RequestBody.create(httpRequest.getContent(), MediaType.get("application/json"));
        Request request = new Request.Builder()
                .url(String.format("http://%s:%s/%s", api, httpRequest.getPort(), httpRequest.getPath()))
                .patch(requestBody)
                .build();
        return httpClient.newCall(request)
                .execute()
                .body()
                .string();
    }

    String delete(UUID id) throws Exception {
        Thread.sleep(500);
        Request request = new Request.Builder()
                .url(String.format("http://%s:8081/api/drugs/%s", api, id))
                .delete()
                .build();
        return httpClient.newCall(request)
                .execute()
                .body()
                .string();
    }

    void delete(HttpRequest httpRequest, UUID id) throws Exception {
        Thread.sleep(500);
        Request request = new Request.Builder()
                .url(String.format("http://%s:%s/%s/%s", api, httpRequest.getPort(), httpRequest.getPath(), id))
                .delete()
                .build();
        httpClient.newCall(request)
                .execute();
    }
}
