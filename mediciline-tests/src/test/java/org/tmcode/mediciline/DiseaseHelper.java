package org.tmcode.mediciline;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.tmcode.mediciline.dto.CreateDiseaseDto;
import org.tmcode.mediciline.dto.DiseaseDto;
import org.tmcode.mediciline.dto.Id;
import org.tmcode.mediciline.dto.UpdateDiseaseDto;

import java.util.List;
import java.util.UUID;

class DiseaseHelper {

    private final HttpConnector httpConnector = new HttpConnector();
    private final DatabaseConnector databaseConnector = new DatabaseConnector();
    private final ObjectMapper objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());;

    UUID createDisease(UUID memberId, CreateDiseaseDto createDiseaseDto) throws Exception {
        String content = objectMapper.writeValueAsString(createDiseaseDto);
        HttpRequest httpRequest = new HttpRequest(8082, String.format("api/members/%s/diseases", memberId), content);
        String response = httpConnector.post(httpRequest);
        return objectMapper.readValue(response, Id.class).getId();
    }

    DiseaseDto getDisease(UUID memberId, UUID diseaseId) throws Exception {
        HttpRequest httpRequest = new HttpRequest(8083, String.format("api/members/%s/diseases/%s", memberId, diseaseId), "");
        String response = httpConnector.get(httpRequest);
        return objectMapper.readValue(response, DiseaseDto.class);
    }

    UUID editDisease(UpdateDiseaseDto updateMemberDto, UUID memberId, UUID diseaseId) throws Exception {
        String content = objectMapper.writeValueAsString(updateMemberDto);
        HttpRequest httpRequest = new HttpRequest(8082, String.format("api/members/%s/diseases/%s", memberId, diseaseId), content);
        String response = httpConnector.put(httpRequest);
        return objectMapper.readValue(response, Id.class).getId();
    }

    void deleteDisease(UUID memberId, UUID diseaseId) throws Exception {
        HttpRequest httpRequest = new HttpRequest(8082, String.format("api/members/%s/diseases/%s", memberId, diseaseId), "");
        httpConnector.delete(httpRequest, memberId);
    }

    UUID finishDisease(UUID memberId, UUID diseaseId) throws Exception {
        HttpRequest httpRequest = new HttpRequest(8082, String.format("api/members/%s/diseases/%s/finish", memberId, diseaseId), "");
        String response = httpConnector.patch(httpRequest);
        return objectMapper.readValue(response, Id.class).getId();
    }

    void clearTable() throws Exception {

        databaseConnector.execute("delete from deleted_entity");
        databaseConnector.execute("delete from member_events");
        databaseConnector.execute("delete from member_status");
        databaseConnector.execute("delete from disease_status");
    }

    List<DiseaseDto> listAllDiseasesByMember(UUID memberId) throws Exception {
        HttpRequest httpRequest = new HttpRequest(8083, String.format("api/members/%s/diseases", memberId), "");
        String response = httpConnector.get(httpRequest);
        TypeReference<List<DiseaseDto>> typeRef = new TypeReference<>() {};
        return objectMapper.readValue(response, typeRef);
    }
}
