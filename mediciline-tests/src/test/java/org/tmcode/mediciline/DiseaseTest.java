package org.tmcode.mediciline;

import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.tmcode.mediciline.dto.CreateDiseaseDto;
import org.tmcode.mediciline.dto.CreateMemberDto;
import org.tmcode.mediciline.dto.DiseaseDto;
import org.tmcode.mediciline.dto.UpdateDiseaseDto;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

class DiseaseTest {

    private DiseaseHelper diseaseHelper;
    private MemberHelper memberHelper;

    @BeforeEach
    public void beforeEach() throws Exception {

        diseaseHelper = new DiseaseHelper();
        memberHelper = new MemberHelper();
        diseaseHelper.clearTable();
    }

    @Test
    public void shouldCreateDisease() throws Exception {

        // given
        UUID memberId = createMember();
        String name = "disease name";
        String description = "disease description";
        LocalDate startDate = LocalDate.of(2020, 1, 1);
        CreateDiseaseDto createDiseaseDto = new CreateDiseaseDto(name, description, startDate);

        //when
        UUID diseaseId = diseaseHelper.createDisease(memberId, createDiseaseDto);

        // then
        DiseaseDto dto = diseaseHelper.getDisease(memberId, diseaseId);
        assertThat(dto.getId()).isEqualTo(diseaseId);
        assertThat(dto.getName()).isEqualTo(name);
        assertThat(dto.getDescription()).isEqualTo(description);
        assertThat(dto.getStartDate()).isEqualTo("2020-01-01");
        assertThat(dto.getEndDate()).isNull();
    }

    @Test
    public void shouldEditDisease() throws Exception {

        // given
        UUID memberId = createMember();
        UUID diseaseId = createDisease(memberId);
        String newName = "new disease name";
        String newDescription = "new disease description";
        LocalDate newStartDate = LocalDate.of(2019, 1, 1);
        UpdateDiseaseDto updateDiseaseDto = new UpdateDiseaseDto(newName, newDescription, newStartDate);

        //when
        UUID uuid = diseaseHelper.editDisease(updateDiseaseDto, memberId, diseaseId);

        // then
        DiseaseDto dto = diseaseHelper.getDisease(memberId, diseaseId);
        assertThat(dto.getId()).isEqualTo(diseaseId);
        assertThat(dto.getId()).isEqualTo(uuid);
        assertThat(dto.getName()).isEqualTo(newName);
        assertThat(dto.getDescription()).isEqualTo(newDescription);
        assertThat(dto.getStartDate()).isEqualTo("2019-01-01");
        assertThat(dto.getEndDate()).isNull();
    }

    @Test
    public void shouldDeleteDisease() throws Exception {

        // given
        UUID memberId = createMember();
        UUID diseaseId = createDisease(memberId);

        // when
        diseaseHelper.deleteDisease(memberId, diseaseId);

        // then
        Assertions.assertThatThrownBy(() -> diseaseHelper.getDisease(memberId, memberId))
                .isInstanceOf(UnrecognizedPropertyException.class);
    }

    @Test
    public void shouldFinishDisease() throws Exception {

        // given
        UUID memberId = createMember();
        UUID diseaseId = createDisease(memberId);

        //when
        diseaseHelper.finishDisease(memberId, diseaseId);

        // then
        DiseaseDto dto = diseaseHelper.getDisease(memberId, diseaseId);
        assertThat(dto.getId()).isEqualTo(diseaseId);
        assertThat(dto.getEndDate()).isEqualTo(LocalDate.now().format(DateTimeFormatter.ISO_DATE));
    }


    @Test
    public void shouldListAllDiseasesByMember() throws Exception {

        // given
        UUID memberId = createMember();
        UUID disease1Id = createDisease(memberId);
        UUID disease2Id = createDisease(memberId);

        // when
        List<DiseaseDto> drugDtos = diseaseHelper.listAllDiseasesByMember(memberId);

        // then
        assertThat(drugDtos).hasSize(2);
    }

    private UUID createMember() throws Exception {
        String name = "my name";
        String birthday = "1984-08-09";
        CreateMemberDto createMemberDto = new CreateMemberDto(name, LocalDate.parse(birthday));
        return memberHelper.createMember(createMemberDto);
    }

    private UUID createDisease(UUID memberId) throws Exception {
        String name = "disease name";
        String description = "disease description";
        LocalDate startDate = LocalDate.of(2020, 1, 1);
        CreateDiseaseDto createDiseaseDto = new CreateDiseaseDto(name, description, startDate);
        return diseaseHelper.createDisease(memberId, createDiseaseDto);
    }
}
