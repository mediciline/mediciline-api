package org.tmcode.mediciline;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.tmcode.mediciline.dto.CreateDrugDto;
import org.tmcode.mediciline.dto.DrugDto;
import org.tmcode.mediciline.dto.Id;
import org.tmcode.mediciline.dto.UpdateDrugDto;

import java.util.List;
import java.util.UUID;

class DrugHelper {

    private final HttpConnector httpConnector = new HttpConnector();
    private final DatabaseConnector databaseConnector = new DatabaseConnector();
    private final ObjectMapper objectMapper = new ObjectMapper();

    UUID createDrug(UUID dictionaryDrugId, String expirationDate) throws Exception {

        CreateDrugDto createDrugDto = new CreateDrugDto(dictionaryDrugId, expirationDate);
        String content = objectMapper.writeValueAsString(createDrugDto);
        HttpRequest httpRequest = new HttpRequest(8081, "api/drugs", content);
        String response = httpConnector.post(httpRequest);

        Id id = objectMapper.readValue(response, Id.class);

        return id.getId();
    }

    DrugDto getDrug(UUID drugId) throws Exception {
        HttpRequest httpRequest = new HttpRequest(8083, String.format("api/drugs/%s", drugId), "");
        String response = httpConnector.get(httpRequest);
        return objectMapper.readValue(response, DrugDto.class);
    }

    UUID editDrug(UUID drugId, String newExpirationDate) throws Exception {
        UpdateDrugDto updateDrugDto = new UpdateDrugDto(newExpirationDate);
        String content = objectMapper.writeValueAsString(updateDrugDto);
        HttpRequest httpRequest = new HttpRequest(8081, String.format("api/drugs/%s", drugId), content);
        String response = httpConnector.put(httpRequest);

        String id = objectMapper.readTree(response).findValue("id").asText();

        return UUID.fromString(id);
    }

    void clearDrugTable() throws Exception {

        databaseConnector.execute("delete from deleted_entity");
        databaseConnector.execute("delete from drug_events");
        databaseConnector.execute("delete from drug_status");
    }

    void createExpiredDrug(UUID drugId) throws Exception {
        databaseConnector.execute("INSERT INTO drug_events (id, drug_id, name, payload, version) VALUES ('" + drugId + "', '0e85e90c-5a47-4590-94f7-633a16c3fea3', 'org.tmcode.mediciline.event.DrugCreatedEvent', '{\"drugId\":\"0e85e90c-5a47-4590-94f7-633a16c3fea3\",\"createdAt\":[2020,3,2,19,10,45,859320000],\"dictionaryDrugId\":\"762df68d-4707-4131-9ddd-b1f8fc9bc460\",\"expirationDate\":{\"date\":[2019,3,31]}}', 1);");
        databaseConnector.execute("INSERT INTO drug_status (id, dictionary_drug_id, expiration_date, expired, meta_data) VALUES ('" + drugId + "', '762df68d-4707-4131-9ddd-b1f8fc9bc460', '2019-03', false, '{\"expired\":[2020,3,2,19,10,45,859320000],\"expirationDate\":[2020,3,2,19,10,45,859320000]}');");
    }

    void checkExpirationDate(UUID drugId) throws Exception {
        httpConnector.patch(drugId);
    }

    void deleteDrug(UUID drugId) throws Exception {
        httpConnector.delete(drugId);
    }

    List<DrugDto> listAllDrugs() throws Exception {
        HttpRequest httpRequest = new HttpRequest(8083, "api/drugs", "");
        String response = httpConnector.get(httpRequest);
        TypeReference<List<DrugDto>> typeRef = new TypeReference<>() {};
        return objectMapper.readValue(response, typeRef);
    }

    List<UUID> listAllExpiredDrugs() throws Exception {
        HttpRequest httpRequest = new HttpRequest(8083, "api/drugs?expired=true", "");
        String response = httpConnector.get(httpRequest);
        TypeReference<List<UUID>> typeRef = new TypeReference<>() {};
        return objectMapper.readValue(response, typeRef);
    }
}
