package org.tmcode.mediciline;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.tmcode.mediciline.dto.CreateMemberDto;
import org.tmcode.mediciline.dto.Id;
import org.tmcode.mediciline.dto.MemberDto;
import org.tmcode.mediciline.dto.UpdateMemberDto;

import java.util.List;
import java.util.UUID;

class MemberHelper {

    private final HttpConnector httpConnector = new HttpConnector();
    private final DatabaseConnector databaseConnector = new DatabaseConnector();
    private final ObjectMapper objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());;

    UUID createMember(CreateMemberDto createMemberDto) throws Exception {

        String content = objectMapper.writeValueAsString(createMemberDto);

        HttpRequest httpRequest = new HttpRequest(8082, "api/members", content);
        String response = httpConnector.post(httpRequest);

        return objectMapper.readValue(response, Id.class).getId();
    }

    MemberDto getMember(UUID memeberId) throws Exception {
        HttpRequest httpRequest = new HttpRequest(8083, String.format("api/members/%s", memeberId), "");
        String response = httpConnector.get(httpRequest);
        return objectMapper.readValue(response, MemberDto.class);
    }

    UUID editMember(UpdateMemberDto updateMemberDto, UUID memberId) throws Exception {
        String content = objectMapper.writeValueAsString(updateMemberDto);
        HttpRequest httpRequest = new HttpRequest(8082, String.format("api/members/%s", memberId), content);
        String response = httpConnector.put(httpRequest);
        return objectMapper.readValue(response, Id.class).getId();
    }

    void clearTable() throws Exception {

        databaseConnector.execute("delete from deleted_entity");
        databaseConnector.execute("delete from member_events");
        databaseConnector.execute("delete from member_status");
    }

    void createExpiredDrug(UUID drugId) throws Exception {
        databaseConnector.execute("INSERT INTO drug_events (id, drug_id, name, payload, version) VALUES ('" + drugId + "', '0e85e90c-5a47-4590-94f7-633a16c3fea3', 'org.tmcode.mediciline.event.DrugCreatedEvent', '{\"drugId\":\"0e85e90c-5a47-4590-94f7-633a16c3fea3\",\"createdAt\":[2020,3,2,19,10,45,859320000],\"dictionaryDrugId\":\"762df68d-4707-4131-9ddd-b1f8fc9bc460\",\"expirationDate\":{\"date\":[2019,3,31]}}', 1);");
        databaseConnector.execute("INSERT INTO drug_status (id, dictionary_drug_id, expiration_date, expired, meta_data) VALUES ('" + drugId + "', '762df68d-4707-4131-9ddd-b1f8fc9bc460', '2019-03', false, '{\"expired\":[2020,3,2,19,10,45,859320000],\"expirationDate\":[2020,3,2,19,10,45,859320000]}');");
    }

    void checkExpirationDate(UUID drugId) throws Exception {
        httpConnector.patch(drugId);
    }

    void deleteMemberId(UUID memberId) throws Exception {
        HttpRequest httpRequest = new HttpRequest(8082, "api/members", "");
        httpConnector.delete(httpRequest, memberId);
    }

    List<MemberDto> listAllMembers() throws Exception {
        HttpRequest httpRequest = new HttpRequest(8083, "api/members", "");
        String response = httpConnector.get(httpRequest);
        TypeReference<List<MemberDto>> typeRef = new TypeReference<>() {};
        return objectMapper.readValue(response, typeRef);
    }
}
