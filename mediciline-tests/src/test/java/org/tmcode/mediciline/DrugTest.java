package org.tmcode.mediciline;

import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.tmcode.mediciline.dto.DrugDto;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import static java.util.UUID.randomUUID;
import static org.assertj.core.api.Assertions.assertThat;

class DrugTest {

    private DrugHelper drugHelper;

    @BeforeEach
    public void beforeEach() throws Exception {

        drugHelper = new DrugHelper();
        drugHelper.clearDrugTable();
    }

    @Test
    public void shouldCreateDrug() throws Exception {

        // given
        UUID dictionaryDrugId = randomUUID();
        String expirationDate = createExpirationDateInFuture(1);

        //when
        UUID drugId = drugHelper.createDrug(dictionaryDrugId, expirationDate);

        // then
        DrugDto drugDto = drugHelper.getDrug(drugId);
        assertThat(drugDto.getId()).isEqualTo(drugId);
        assertThat(drugDto.getDictionaryDrugId()).isEqualTo(dictionaryDrugId);
        assertThat(drugDto.getExpirationDate()).isEqualTo(expirationDate);
        assertThat(drugDto.isExpired()).isFalse();
    }

    @Test
    public void shouldEditDrug() throws Exception {

        // given
        UUID dictionaryDrugId = randomUUID();
        String expirationDate = createExpirationDateInFuture(1);
        UUID drugId = drugHelper.createDrug(dictionaryDrugId, expirationDate);

        String newExpirationDate = createExpirationDateInFuture(2);

        //when
        UUID uuid = drugHelper.editDrug(drugId, newExpirationDate);

        // then
        DrugDto drugDto = drugHelper.getDrug(drugId);
        assertThat(drugDto.getId()).isEqualTo(drugId);
        assertThat(drugDto.getId()).isEqualTo(uuid);
        assertThat(drugDto.getDictionaryDrugId()).isEqualTo(dictionaryDrugId);
        assertThat(drugDto.isExpired()).isFalse();
        assertThat(drugDto.getExpirationDate()).isEqualTo(newExpirationDate);
    }

    @Test
    public void shouldCheckExpirationDate() throws Exception {

    	// given
        UUID drugId = UUID.fromString("0e85e90c-5a47-4590-94f7-633a16c3fea3");
        drugHelper.createExpiredDrug(drugId);

        assertThat(drugHelper.getDrug(drugId).isExpired()).isFalse();

    	// when
        drugHelper.checkExpirationDate(drugId);

    	// then
        DrugDto drugDto = drugHelper.getDrug(drugId);
        assertThat(drugDto.getId()).isEqualTo(drugId);
        assertThat(drugDto.isExpired()).isTrue();
    }

    @Test
    public void shouldDeleteDrug() throws Exception {

        // given
        UUID drugId = drugHelper.createDrug(randomUUID(), createExpirationDateInFuture(1));

        // when
        drugHelper.deleteDrug(drugId);

        // then
        Assertions.assertThatThrownBy(() -> drugHelper.getDrug(drugId))
                .isInstanceOf(UnrecognizedPropertyException.class);
    }

    @Test
    public void shouldListAllDrugs() throws Exception {

        // given
        drugHelper.createDrug(randomUUID(), createExpirationDateInFuture(1));
        drugHelper.createDrug(randomUUID(), createExpirationDateInFuture(1));

        // when
        List<DrugDto> drugDtos = drugHelper.listAllDrugs();

        // then
        assertThat(drugDtos).hasSize(2);
    }

    @Test
    public void shouldListAllExpiredDrugs() throws Exception {

        // given
        drugHelper.createDrug(randomUUID(), createExpirationDateInFuture(1));
        drugHelper.createDrug(randomUUID(), createExpirationDateInPast(1));
        drugHelper.createDrug(randomUUID(), createExpirationDateInPast(1));

        // when
        List<UUID> drugDtos = drugHelper.listAllExpiredDrugs();

        // then
        assertThat(drugDtos).hasSize(2);
    }

    private String createExpirationDateInFuture(long days) {
        LocalDate now = LocalDate.now().plusDays(days);
        int year = now.getYear();
        String month = getMonth(now);

        return year + "-" + month;
    }

    private String createExpirationDateInPast(long months) {
        LocalDate now = LocalDate.now().minusMonths(months);
        int year = now.getYear();
        String month = getMonth(now);

        return year + "-" + month;
    }

    private String getMonth(LocalDate now) {
        int monthValue = now.getMonthValue();
        if (monthValue < 10) {
            return "0" + monthValue;
        }

        return "" + monthValue;
    }
}
