package org.tmcode.mediciline.domain;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.tmcode.mediciline.event.DrugCreatedEvent;
import org.tmcode.mediciline.event.DrugDeletedEvent;
import org.tmcode.mediciline.event.DrugExpiredEvent;
import org.tmcode.mediciline.event.DrugNotExpiredEvent;
import org.tmcode.mediciline.event.DrugUpdatedEvent;
import org.tmcode.mediciline.vo.ExpirationDate;

import java.util.UUID;

import static java.time.LocalDateTime.now;
import static java.util.UUID.randomUUID;
import static org.assertj.core.api.Assertions.assertThat;

class DrugTest {

    @Test
    public void shouldCreateDrug() {

    	// given
        UUID drugId = randomUUID();
        UUID dictionaryDrugId = randomUUID();
        ExpirationDate expirationDate = new ExpirationDate("2020-01");
        DrugCreatedEvent event = new DrugCreatedEvent(drugId, now(), dictionaryDrugId, expirationDate);

        // when
        Drug drug = new Drug(event);

        // then
        assertThat(drug).hasFieldOrPropertyWithValue("aggregateId", drugId);
        assertThat(drug).hasFieldOrPropertyWithValue("dictionaryDrugId", dictionaryDrugId);
        assertThat(drug).hasFieldOrPropertyWithValue("expirationDate", expirationDate);
        assertThat(drug).hasFieldOrPropertyWithValue("isAvailable", true);
    }

    @Test
    public void shouldApplyDrugUpdatedEvent() {

        // given
        UUID drugId = randomUUID();
        ExpirationDate expirationDate = new ExpirationDate("2020-01");

        Drug drug = createDrug(drugId, expirationDate);

        ExpirationDate newExpirationDate = new ExpirationDate("2022-01");

        DrugUpdatedEvent event = new DrugUpdatedEvent(drugId,   now(), newExpirationDate);

        // when
        drug.apply(event);

        // then
        assertThat(drug).hasFieldOrPropertyWithValue("aggregateId", drugId);
        assertThat(drug).hasFieldOrPropertyWithValue("expirationDate", newExpirationDate);
    }

    @Test
    public void shouldApplyDrugDeletedEvent() {

        // given
        UUID drugId = randomUUID();

        Drug drug = createDrug(drugId);

        DrugDeletedEvent event = new DrugDeletedEvent(drugId, now());

        // when
        drug.apply(event);

        // then
        assertThat(drug).hasFieldOrPropertyWithValue("aggregateId", drugId);
        assertThat(drug).hasFieldOrPropertyWithValue("isAvailable", false);
    }

    @Test
    public void shouldThrowExceptionWhenUpdateNotAvailableDrug() {

        // given
        UUID drugId = randomUUID();

        Drug drug = createDrug(drugId);
        drug.apply(new DrugDeletedEvent(drugId, now()));

        ExpirationDate newExpirationDate = new ExpirationDate("2022-01");

        DrugUpdatedEvent event = new DrugUpdatedEvent(drugId, now(), newExpirationDate);

        //when
        Assertions.assertThatThrownBy(() -> drug.apply(event))

        //then
        .isInstanceOf(CanNotModifyDeletedDrugException.class);
    }

    @Test
    public void shouldApplyDrugExpiredEvent() {

        // given
        UUID drugId = randomUUID();
        Drug drug = createDrug(drugId);

        DrugExpiredEvent event = new DrugExpiredEvent(drugId, now());

        //when
        drug.apply(event);

        //then
        assertThat(drug).hasFieldOrPropertyWithValue("aggregateId", drugId);
        assertThat(drug).hasFieldOrPropertyWithValue("isExpired", true);
    }

    @Test
    public void shouldApplyDrugNotExpiredEvent() {

        // given
        UUID drugId = randomUUID();
        Drug drug = createDrug(drugId);

        drug.apply(new DrugExpiredEvent(drugId, now()));
        DrugNotExpiredEvent event = new DrugNotExpiredEvent(drugId, now());

        //when
        drug.apply(event);

        //then
        assertThat(drug).hasFieldOrPropertyWithValue("aggregateId", drugId);
        assertThat(drug).hasFieldOrPropertyWithValue("isExpired", false);
    }

    private Drug createDrug(UUID drugId, ExpirationDate expirationDate) {
        DrugCreatedEvent event = new DrugCreatedEvent(drugId, now(), randomUUID(), expirationDate);
        return new Drug(event);
    }

    private Drug createDrug(UUID drugId) {
        return createDrug(drugId, new ExpirationDate("2020-01"));
    }

}
