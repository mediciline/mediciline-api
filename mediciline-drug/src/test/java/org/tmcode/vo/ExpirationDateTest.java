package org.tmcode.vo;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.tmcode.mediciline.vo.ExpirationDate;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

class ExpirationDateTest {

    @ParameterizedTest
    @MethodSource
    public void shouldCreateExpirationDate(String date) {

        // given

        // when
        ExpirationDate expirationDate = new ExpirationDate(date);

        // then
        Assertions.assertThat(expirationDate.obtainValue()).isEqualTo(date);
    }

    private static Stream<Arguments> shouldCreateExpirationDate() {
        return Stream.of(
                Arguments.of("2020-01"),
                Arguments.of("1999-12"),
                Arguments.of("2023-06"),
                Arguments.of("2050-12")
        );
    }

    @ParameterizedTest
    @MethodSource
    public void shouldThrowExceptionWhenCreateExpirationDate(String date) {

        // given

        //when
        assertThatThrownBy(() -> new ExpirationDate(date))

                //then
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("invalid.date.format");
    }

    private static Stream<Arguments> shouldThrowExceptionWhenCreateExpirationDate() {
        return Stream.of(
                Arguments.of("2020-1"),
                Arguments.of("99-12"),
                Arguments.of("2023-13"),
                Arguments.of("2050-121")
        );
    }
}
