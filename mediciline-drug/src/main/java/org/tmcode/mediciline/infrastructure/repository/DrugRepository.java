package org.tmcode.mediciline.infrastructure.repository;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.tmcode.mediciline.domain.Drug;
import org.tmcode.mediciline.event.DrugCreatedEvent;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.UUID;

@Component
class DrugRepository implements org.tmcode.mediciline.domain.DrugRepository {

    private final EventRepository eventRepository;
    private final EntityManager entityManager;
    private final JsonFactory jsonFactory;

    @Autowired
    DrugRepository(EventRepository eventRepository, EntityManager entityManager, JsonFactory jsonFactory) {
        this.eventRepository = eventRepository;
        this.entityManager = entityManager;
        this.jsonFactory = jsonFactory;
    }

    @SneakyThrows
    @SuppressWarnings("unchecked")
    @Override
    public Drug get(UUID drugId) {
        List<Event> events = entityManager.createQuery("select e from Event e where e.drugId = :drugId order by e.version asc")
                .setParameter("drugId", drugId)
                .getResultList();

        if (events == null || events.isEmpty()) {
            throw new IllegalStateException();
        }

        Event event = events.get(0);
        String name = event.getName();
        Class<?> clazz = Class.forName(name);

        if (clazz != DrugCreatedEvent.class) {
            throw new IllegalStateException();
        }
        Object payload = readPayload(event);
        Drug drug = new Drug(payload);

        events.stream()
                .skip(1)
                .map(this::readPayload)
                .forEach(drug::apply);

        return drug;
    }

    @SneakyThrows
    private Object readPayload(Event event) {
        String payload = event.getPayload();
        Class<?> clazz = Class.forName(event.getName());
        return jsonFactory.readJson(payload, clazz);
    }

    @Override
    @Transactional
    public void save(Drug drug) {
        drug.listEvents().forEach((version, event) -> save(version, event, drug.getAggregateId()));
    }

    private void save(Long version, Object event, UUID drugId) {
        if (isNotExists(version, event, drugId)) {
            String payload = jsonFactory.createJson(event);
            eventRepository.save(new Event(UUID.randomUUID(), drugId, version, event.getClass().getName(), payload));
        }
    }

    private boolean isNotExists(Long version, Object event, UUID drugId) {
        Long singleResult = (Long) entityManager.createQuery("select count(e.id) " +
                "from Event e " +
                "where e.version = :version and e.name = :name and e.drugId = :drugId")
                .setParameter("version", version)
                .setParameter("name", event.getClass().getName())
                .setParameter("drugId", drugId)
                .setMaxResults(1)
                .getSingleResult();

        return singleResult == 0;
    }
}
