package org.tmcode.mediciline.infrastructure.repository;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

@Component
class JsonFactory {

    private final ObjectMapper objectMapper = new ObjectMapper()
            .setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY)
            .registerModule(new JavaTimeModule());

    @SneakyThrows
    public String createJson(Object event) {
        return objectMapper.writer().writeValueAsString(event);
    }

    @SneakyThrows
    public <E> E readJson(String payload, Class<E> clazz) {
        return objectMapper.reader().forType(clazz).readValue(payload);
    }
}
