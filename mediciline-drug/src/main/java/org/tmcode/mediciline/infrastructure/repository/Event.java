package org.tmcode.mediciline.infrastructure.repository;

import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "drug_events")
public class Event {

    @Id
    @Type(type = "uuid-char")
    private UUID id;
    @Type(type = "uuid-char")
    private UUID drugId;
    private Long version;
    private String name;
    @Type(type="text")
    private String payload;

    private Event() {
    }

    public Event(UUID id, UUID drugId, Long version, String name, String payload) {
        this.id = id;
        this.drugId = drugId;
        this.version = version;
        this.name = name;
        this.payload = payload;
    }

    public String getName() {
        return name;
    }

    public String getPayload() {
        return payload;
    }
}
