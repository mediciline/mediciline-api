package org.tmcode.mediciline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class DrugStartApplication {

    public static void main(String[] args) {
        SpringApplication.run(DrugStartApplication.class, args);
    }
}
