package org.tmcode.mediciline.application.command;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.tmcode.mediciline.vo.ExpirationDate;

import java.util.UUID;

@AllArgsConstructor
@Getter
public class CreateDrugCommand {

    private final UUID drugId;
    private final UUID dictionaryDrugId;
    private final ExpirationDate expirationDate;
}
