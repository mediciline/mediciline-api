package org.tmcode.mediciline.application.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tmcode.cqrs.CommandHandler;
import org.tmcode.mediciline.domain.Drug;
import org.tmcode.mediciline.domain.DrugRepository;
import org.tmcode.mediciline.domain.EventSender;
import org.tmcode.mediciline.event.DrugDeletedEvent;

import java.util.UUID;

import static java.time.LocalDateTime.now;

@Component
class DeleteDrugHandler implements CommandHandler<DeleteDrugCommand> {

    private final DrugRepository drugRepository;
    private final EventSender eventSender;

    @Autowired
    DeleteDrugHandler(DrugRepository drugRepository, EventSender eventSender) {
        this.drugRepository = drugRepository;
        this.eventSender = eventSender;
    }

    @Override
    public void handle(DeleteDrugCommand command) {

        UUID drugId = command.getDrugId();

        DrugDeletedEvent event = new DrugDeletedEvent(drugId, now());
        Drug drug = drugRepository.get(drugId);
        drug.apply(event);
        drugRepository.save(drug);

        eventSender.send(event);
    }
}
