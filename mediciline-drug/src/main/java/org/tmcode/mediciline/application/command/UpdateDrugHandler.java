package org.tmcode.mediciline.application.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tmcode.cqrs.CommandHandler;
import org.tmcode.mediciline.domain.Drug;
import org.tmcode.mediciline.domain.DrugRepository;
import org.tmcode.mediciline.domain.EventSender;
import org.tmcode.mediciline.event.DrugUpdatedEvent;
import org.tmcode.mediciline.vo.ExpirationDate;

import java.util.UUID;

import static java.time.LocalDateTime.now;

@Component
class UpdateDrugHandler implements CommandHandler<UpdateDrugCommand> {

    private final DrugRepository drugRepository;
    private final EventSender eventSender;

    @Autowired
    UpdateDrugHandler(DrugRepository drugRepository, EventSender eventSender) {
        this.drugRepository = drugRepository;
        this.eventSender = eventSender;
    }

    @Override
    public void handle(UpdateDrugCommand command) {

        UUID drugId = command.getDrugId();
        ExpirationDate expirationDate = command.getExpirationDate();

        DrugUpdatedEvent event = new DrugUpdatedEvent(drugId, now(), expirationDate);
        Drug drug = drugRepository.get(drugId);
        drug.apply(event);
        drugRepository.save(drug);

        eventSender.send(event);
    }
}
