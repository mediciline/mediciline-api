package org.tmcode.mediciline.application.command;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tmcode.cqrs.CommandHandler;
import org.tmcode.mediciline.domain.Drug;
import org.tmcode.mediciline.domain.DrugRepository;
import org.tmcode.mediciline.domain.EventSender;
import org.tmcode.mediciline.event.DrugCreatedEvent;
import org.tmcode.mediciline.event.DrugExpiredEvent;
import org.tmcode.mediciline.event.DrugNotExpiredEvent;
import org.tmcode.mediciline.event.DrugUpdatedEvent;

import java.util.UUID;

import static java.time.LocalDateTime.now;
import static org.tmcode.mediciline.event.QueuesDefinition.*;

@Component
class CheckExpirationDateHandler implements CommandHandler<CheckExpirationDateCommand> {

    private final DrugRepository drugRepository;
    private final EventSender eventSender;

    @Autowired
    CheckExpirationDateHandler(DrugRepository drugRepository, EventSender eventSender) {
        this.drugRepository = drugRepository;
        this.eventSender = eventSender;
    }

    @Override
    public void handle(CheckExpirationDateCommand command) {

        UUID drugId = command.getDrugId();

        Drug drug = drugRepository.get(drugId);

        if (drug.hasExpired()) {
            processEvent(drug, new DrugExpiredEvent(drugId, now()));
        } else if(drug.isNoLongerExpired()) {
            processEvent(drug, new DrugNotExpiredEvent(drugId, now()));
        }
    }

    private void processEvent(Drug drug, Object event) {
        drug.apply(event);
        drugRepository.save(drug);
        eventSender.send(event);
    }

    @RabbitListener(queues = {DRUG_CREATED_EVENT_CHECK_EXPIRATION_DATE_HANDLER, DRUG_CREATED_EVENT_CHECK_EXPIRATION_DATE_HANDLER_RETRY})
    public void on(DrugCreatedEvent event) {
        handle(new CheckExpirationDateCommand(event.getDrugId()));
    }

    @RabbitListener(queues = {DRUG_UPDATED_EVENT_CHECK_EXPIRATION_DATE_HANDLER, DRUG_UPDATED_EVENT_CHECK_EXPIRATION_DATE_HANDLER_RETRY})
    public void on(DrugUpdatedEvent event) {
        handle(new CheckExpirationDateCommand(event.getDrugId()));
    }
}
