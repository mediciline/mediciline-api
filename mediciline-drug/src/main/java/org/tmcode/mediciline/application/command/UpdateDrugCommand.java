package org.tmcode.mediciline.application.command;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.tmcode.mediciline.vo.ExpirationDate;

import java.util.UUID;

@AllArgsConstructor
@Getter
public class UpdateDrugCommand {

    private final UUID drugId;
    private final ExpirationDate expirationDate;
}
