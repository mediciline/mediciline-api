package org.tmcode.mediciline.application.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tmcode.cqrs.CommandHandler;
import org.tmcode.mediciline.domain.Drug;
import org.tmcode.mediciline.domain.DrugRepository;
import org.tmcode.mediciline.domain.EventSender;
import org.tmcode.mediciline.event.DrugCreatedEvent;
import org.tmcode.mediciline.vo.ExpirationDate;

import java.util.UUID;

import static java.time.LocalDateTime.now;

@Component
class CreateDrugHandler implements CommandHandler<CreateDrugCommand> {

    private final DrugRepository drugRepository;
    private final EventSender eventSender;

    @Autowired
    CreateDrugHandler(DrugRepository drugRepository, EventSender eventSender) {
        this.drugRepository = drugRepository;
        this.eventSender = eventSender;
    }

    @Override
    public void handle(CreateDrugCommand command) {

        UUID drugId = command.getDrugId();
        UUID dictionaryDrugId = command.getDictionaryDrugId();
        ExpirationDate expirationDate = command.getExpirationDate();

        DrugCreatedEvent event = new DrugCreatedEvent(drugId, now(),dictionaryDrugId, expirationDate);
        Drug drug = new Drug(event);

        drugRepository.save(drug);

        eventSender.send(event);
    }
}
