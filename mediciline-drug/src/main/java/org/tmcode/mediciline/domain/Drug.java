package org.tmcode.mediciline.domain;

import com.google.common.collect.ImmutableMap;
import org.tmcode.mediciline.event.DrugCreatedEvent;
import org.tmcode.mediciline.event.DrugDeletedEvent;
import org.tmcode.mediciline.event.DrugExpiredEvent;
import org.tmcode.mediciline.event.DrugNotExpiredEvent;
import org.tmcode.mediciline.event.DrugUpdatedEvent;
import org.tmcode.mediciline.vo.ExpirationDate;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import static java.time.LocalDate.now;
import static org.tmcode.mediciline.utills.AssertUtils.isTrue;

public class Drug {

    private UUID aggregateId;
    private UUID dictionaryDrugId;
    private ExpirationDate expirationDate;
    private boolean isExpired = false;
    private boolean isAvailable = false;
    private final Map<Long, Object> events = new ConcurrentHashMap<>();;
    private final AtomicLong version = new AtomicLong(0);

    public Drug(Object event) {
        apply(event);
    }

    public void apply(Object event) {
        if (event instanceof DrugCreatedEvent) {
            applyDrugCreated((DrugCreatedEvent) event);
        }
        isTrue(isAvailable, isAvailable -> isAvailable, CanNotModifyDeletedDrugException::new);
        if (event instanceof DrugUpdatedEvent) {
            applyDrugUpdatedEvent((DrugUpdatedEvent) event);
        }
        if (event instanceof DrugDeletedEvent) {
            applyDrugDeletedEvent((DrugDeletedEvent) event);
        }
        if (event instanceof DrugExpiredEvent) {
            applyDrugExpiredEvent((DrugExpiredEvent) event);
        }
        if (event instanceof DrugNotExpiredEvent) {
            applyDrugNotExpiredEvent((DrugNotExpiredEvent) event);
        }

        long version = this.version.incrementAndGet();
        events.put(version, event);
    }

    private void applyDrugCreated(DrugCreatedEvent event) {
        this.aggregateId = event.getDrugId();
        this.dictionaryDrugId = event.getDictionaryDrugId();
        this.expirationDate = event.getExpirationDate();
        this.isAvailable = true;
    }

    private void applyDrugUpdatedEvent(DrugUpdatedEvent event) {
        this.expirationDate = event.getExpirationDate();
    }

    private void applyDrugDeletedEvent(DrugDeletedEvent event) {
        this.isAvailable = false;
    }

    private void applyDrugExpiredEvent(DrugExpiredEvent event) {
        this.isExpired = true;
    }

    private void applyDrugNotExpiredEvent(DrugNotExpiredEvent event) {
        this.isExpired = false;
    }

    public Map<Long, Object> listEvents() {
        return ImmutableMap.copyOf(events);
    }

    public UUID getAggregateId() {
        return aggregateId;
    }

    public boolean hasExpired() {
        return !isExpired && expirationDate.isBefore(now());
    }

    public boolean isNoLongerExpired() {
        return isExpired && expirationDate.isAfter(now());
    }
}
