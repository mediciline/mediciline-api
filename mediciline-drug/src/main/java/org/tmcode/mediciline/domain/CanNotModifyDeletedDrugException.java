package org.tmcode.mediciline.domain;

public class CanNotModifyDeletedDrugException extends RuntimeException {
    public CanNotModifyDeletedDrugException() {
        super("CanNotModifyDeletedDrug");
    }
}
