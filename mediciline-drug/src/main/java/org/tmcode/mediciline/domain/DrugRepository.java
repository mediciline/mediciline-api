package org.tmcode.mediciline.domain;

import java.util.UUID;

public interface DrugRepository {
    Drug get(UUID drugId);

    void save(Drug drug);
}
