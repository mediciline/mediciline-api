package org.tmcode.mediciline.domain;

public interface EventSender {
    void send(Object event);
}
