package org.tmcode.mediciline.interfaces.cron;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tmcode.cqrs.Gate;


@Service
class DrugOverdueScheduledTasks {

    private final Gate gate;

    @Autowired
    DrugOverdueScheduledTasks(Gate gate) {
        this.gate = gate;
    }


    @SneakyThrows
//    @Scheduled(fixedRate = 2000)
    public void processDrugs() {

//        drugFinder.findByQuery("findAllNotExpired")
//                .forEach(uuid -> gate.dispatch(new CheckExpirationDateCommand(uuid)));
    }
}
