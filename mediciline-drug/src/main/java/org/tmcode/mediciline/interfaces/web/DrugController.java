package org.tmcode.mediciline.interfaces.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tmcode.cqrs.Gate;
import org.tmcode.mediciline.application.command.CheckExpirationDateCommand;
import org.tmcode.mediciline.application.command.CreateDrugCommand;
import org.tmcode.mediciline.application.command.DeleteDrugCommand;
import org.tmcode.mediciline.application.command.UpdateDrugCommand;
import org.tmcode.mediciline.dto.CreateDrugDto;
import org.tmcode.mediciline.dto.Id;
import org.tmcode.mediciline.dto.UpdateDrugDto;
import org.tmcode.mediciline.vo.ExpirationDate;

import java.util.UUID;

@RestController
@RequestMapping("/api/drugs")
class DrugController {

    private final Gate gate;

    @Autowired
    DrugController(Gate gate) {
        this.gate = gate;
    }

    @PostMapping
    public Id createDrug(@RequestBody CreateDrugDto dto) {
        return createDrug(UUID.randomUUID(), dto);
    }

    @PostMapping(path = "{drugId}")
    public Id createDrug(@PathVariable UUID drugId, @RequestBody CreateDrugDto dto) {
        UUID dictionaryDrugId = dto.getDictionaryDrugId();
        ExpirationDate expirationDate = new ExpirationDate(dto.getExpirationDate());
        gate.dispatch(new CreateDrugCommand(drugId, dictionaryDrugId, expirationDate));
        return new Id(drugId);
    }

    @PutMapping(path = "{drugId}")
    public Id updateDrug(@PathVariable UUID drugId, @RequestBody UpdateDrugDto dto) {
        ExpirationDate expirationDate = new ExpirationDate(dto.getExpirationDate());
        gate.dispatch(new UpdateDrugCommand(drugId, expirationDate));
        return new Id(drugId);
    }

    @PatchMapping(path = "{drugId}")
    public Id checkExpirationDate(@PathVariable UUID drugId) {
        gate.dispatch(new CheckExpirationDateCommand(drugId));
        return new Id(drugId);
    }

    @DeleteMapping(path = "{drugId}")
    public Id deleteDrug(@PathVariable UUID drugId) {
        gate.dispatch(new DeleteDrugCommand(drugId));
        return new Id(drugId);
    }
}
