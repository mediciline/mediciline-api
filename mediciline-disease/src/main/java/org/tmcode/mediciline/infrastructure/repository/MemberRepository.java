package org.tmcode.mediciline.infrastructure.repository;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.tmcode.mediciline.domain.Member;
import org.tmcode.mediciline.event.MemberCreatedEvent;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.UUID;

@Component
class MemberRepository implements org.tmcode.mediciline.domain.MemberRepository {

    private final EventRepository eventRepository;
    private final EntityManager entityManager;
    private final JsonFactory jsonFactory;

    @Autowired
    MemberRepository(EventRepository eventRepository, EntityManager entityManager, JsonFactory jsonFactory) {
        this.eventRepository = eventRepository;
        this.entityManager = entityManager;
        this.jsonFactory = jsonFactory;
    }

    @SneakyThrows
    @SuppressWarnings("unchecked")
    @Override
    public Member get(UUID memberId) {
        List<Event> events = entityManager.createQuery("select e from Event e where e.memberId = :memberId order by e.version asc")
                .setParameter("memberId", memberId)
                .getResultList();

        if (events == null || events.isEmpty()) {
            throw new IllegalStateException();
        }

        Event event = events.get(0);
        String name = event.getName();
        Class<?> clazz = Class.forName(name);

        if (clazz != MemberCreatedEvent.class) {
            throw new IllegalStateException();
        }
        Object payload = readPayload(event);
        Member member = new Member(payload);

        events.stream()
                .skip(1)
                .map(this::readPayload)
                .forEach(member::apply);

        return member;
    }

    @SneakyThrows
    private Object readPayload(Event event) {
        String payload = event.getPayload();
        Class<?> clazz = Class.forName(event.getName());
        return jsonFactory.readJson(payload, clazz);
    }

    @Override
    @Transactional
    public void save(Member member) {
        member.listEvents().forEach((version, event) -> save(version, event, member.getId()));
    }

    private void save(Long version, Object event, UUID memberId) {
        if (isNotExists(version, event, memberId)) {
            String payload = jsonFactory.createJson(event);
            eventRepository.save(new Event(UUID.randomUUID(), memberId, version, event.getClass().getName(), payload));
        }
    }

    private boolean isNotExists(Long version, Object event, UUID memberId) {
        Long singleResult = (Long) entityManager.createQuery("select count(e.id) " +
                "from Event e " +
                "where e.version = :version and e.name = :name and e.memberId = :memberId")
                .setParameter("version", version)
                .setParameter("name", event.getClass().getName())
                .setParameter("memberId", memberId)
                .setMaxResults(1)
                .getSingleResult();

        return singleResult == 0;
    }
}
