package org.tmcode.mediciline.infrastructure.queue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tmcode.event.MessageSender;
import org.tmcode.mediciline.domain.EventSender;

@Component
class RabbitMessageSender implements EventSender {

    private final MessageSender messageSender;

    @Autowired
    RabbitMessageSender(MessageSender messageSender) {
        this.messageSender = messageSender;
    }

    @Override
    public void send(Object event) {
        messageSender.send(event);
    }
}
