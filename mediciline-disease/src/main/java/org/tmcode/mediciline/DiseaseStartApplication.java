package org.tmcode.mediciline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DiseaseStartApplication {

    public static void main(String[] args) {
        SpringApplication.run(DiseaseStartApplication.class, args);
    }
}
