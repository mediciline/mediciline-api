package org.tmcode.mediciline.interfaces.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tmcode.cqrs.Gate;
import org.tmcode.mediciline.application.command.CreateDiseaseCommand;
import org.tmcode.mediciline.application.command.CreateMemberCommand;
import org.tmcode.mediciline.application.command.DeleteDiseaseCommand;
import org.tmcode.mediciline.application.command.DeleteMemberCommand;
import org.tmcode.mediciline.application.command.FinishDiseaseCommand;
import org.tmcode.mediciline.application.command.UpdateDiseaseCommand;
import org.tmcode.mediciline.application.command.UpdateMemberCommand;
import org.tmcode.mediciline.dto.CreateDiseaseDto;
import org.tmcode.mediciline.dto.CreateMemberDto;
import org.tmcode.mediciline.dto.Id;
import org.tmcode.mediciline.dto.UpdateDiseaseDto;
import org.tmcode.mediciline.dto.UpdateMemberDto;

import java.util.UUID;

@RestController
@RequestMapping("/api/members")
class MemberController {

    private final Gate gate;

    @Autowired
    MemberController(Gate gate) {
        this.gate = gate;
    }

    @PostMapping
    public Id createMember(@RequestBody CreateMemberDto dto) {
        return createMember(UUID.randomUUID(), dto);
    }

    @PostMapping(path = "{memberId}")
    public Id createMember(@PathVariable UUID memberId, @RequestBody CreateMemberDto dto) {
        gate.dispatch(new CreateMemberCommand(memberId, dto.getName(), dto.getBirthday()));
        return new Id(memberId);
    }

    @PutMapping(path = "{memberId}")
    public Id updateMember(@PathVariable UUID memberId, @RequestBody UpdateMemberDto dto) {
        gate.dispatch(new UpdateMemberCommand(memberId, dto.getName(), dto.getBirthday()));
        return new Id(memberId);
    }

    @DeleteMapping(path = "{memberId}")
    public Id deleteMember(@PathVariable UUID memberId) {
        gate.dispatch(new DeleteMemberCommand(memberId));
        return new Id(memberId);
    }

    @PostMapping(path = "{memberId}/diseases")
    public Id createDisease(@PathVariable UUID memberId, @RequestBody CreateDiseaseDto dto) {
        return createDisease(UUID.randomUUID(), memberId, dto);
    }

    @PostMapping(path = "{memberId}/diseases/{diseaseId}")
    public Id createDisease(@PathVariable UUID diseaseId, @PathVariable UUID memberId, @RequestBody CreateDiseaseDto dto) {
        gate.dispatch(new CreateDiseaseCommand(diseaseId, memberId, dto.getName(), dto.getDescription(), dto.getStartDate()));
        return new Id(diseaseId);
    }

    @PutMapping(path = "{memberId}/diseases/{diseaseId}")
    public Id updateDisease(@PathVariable UUID diseaseId, @PathVariable UUID memberId, @RequestBody UpdateDiseaseDto dto) {
        gate.dispatch(new UpdateDiseaseCommand(diseaseId, memberId, dto.getName(), dto.getDescription(), dto.getStartDate()));
        return new Id(diseaseId);
    }

    @DeleteMapping(path = "{memberId}/diseases/{diseaseId}")
    public Id deleteDisease(@PathVariable UUID diseaseId, @PathVariable UUID memberId) {
        gate.dispatch(new DeleteDiseaseCommand(memberId, diseaseId));
        return new Id(diseaseId);
    }

    @PatchMapping(path = "{memberId}/diseases/{diseaseId}/finish")
    public Id finishDisease(@PathVariable UUID diseaseId, @PathVariable UUID memberId) {
        gate.dispatch(new FinishDiseaseCommand(memberId, diseaseId));
        return new Id(diseaseId);
    }
}
