package org.tmcode.mediciline.application.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tmcode.cqrs.CommandHandler;
import org.tmcode.mediciline.domain.EventSender;
import org.tmcode.mediciline.domain.Member;
import org.tmcode.mediciline.domain.MemberRepository;
import org.tmcode.mediciline.event.MemberUpdatedEvent;

import java.time.LocalDate;
import java.util.UUID;

import static java.time.LocalDateTime.now;

@Component
class UpdateMemberHandler implements CommandHandler<UpdateMemberCommand> {

    private final MemberRepository memberRepository;
    private final EventSender eventSender;

    @Autowired
    UpdateMemberHandler(MemberRepository memberRepository, EventSender eventSender) {
        this.memberRepository = memberRepository;
        this.eventSender = eventSender;
    }

    @Override
    public void handle(UpdateMemberCommand command) {

        UUID memberId = command.getMemberId();
        String name = command.getName();
        LocalDate birthday = command.getBirthday();

        Member member = memberRepository.get(memberId);

        MemberUpdatedEvent event = new MemberUpdatedEvent(memberId, now(), name, birthday);
        member.apply(event);

        memberRepository.save(member);
        eventSender.send(event);
    }
}
