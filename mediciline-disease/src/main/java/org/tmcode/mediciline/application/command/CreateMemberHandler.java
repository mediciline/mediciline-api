package org.tmcode.mediciline.application.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tmcode.cqrs.CommandHandler;
import org.tmcode.mediciline.domain.EventSender;
import org.tmcode.mediciline.domain.Member;
import org.tmcode.mediciline.domain.MemberRepository;
import org.tmcode.mediciline.event.MemberCreatedEvent;

import java.time.LocalDate;
import java.util.UUID;

import static java.time.LocalDateTime.now;

@Component
class CreateMemberHandler implements CommandHandler<CreateMemberCommand> {

    private final MemberRepository memberRepository;
    private final EventSender eventSender;

    @Autowired
    CreateMemberHandler(MemberRepository memberRepository, EventSender eventSender) {
        this.memberRepository = memberRepository;
        this.eventSender = eventSender;
    }

    @Override
    public void handle(CreateMemberCommand command) {

        UUID memberId = command.getMemberId();
        String name = command.getName();
        LocalDate birthday = command.getBirthday();

        MemberCreatedEvent event = new MemberCreatedEvent(memberId, now(), name, birthday);
        Member member = new Member(event);
        memberRepository.save(member);
        eventSender.send(event);
    }
}
