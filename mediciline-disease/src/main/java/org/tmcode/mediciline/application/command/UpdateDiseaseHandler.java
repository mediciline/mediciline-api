package org.tmcode.mediciline.application.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tmcode.cqrs.CommandHandler;
import org.tmcode.mediciline.event.DiseaseUpdatedEvent;
import org.tmcode.mediciline.domain.EventSender;
import org.tmcode.mediciline.domain.Member;
import org.tmcode.mediciline.domain.MemberRepository;

import java.time.LocalDate;
import java.util.UUID;

import static java.time.LocalDateTime.now;

@Component
class UpdateDiseaseHandler implements CommandHandler<UpdateDiseaseCommand> {

    private final MemberRepository memberRepository;
    private final EventSender eventSender;

    @Autowired
    UpdateDiseaseHandler(MemberRepository memberRepository, EventSender eventSender) {
        this.memberRepository = memberRepository;
        this.eventSender = eventSender;
    }

    @Override
    public void handle(UpdateDiseaseCommand command) {

        UUID memberId = command.getMemberId();
        UUID diseaseId = command.getDiseaseId();
        String name = command.getName();
        String description = command.getDescription();
        LocalDate startDate = command.getStartDate();

        Member member = memberRepository.get(memberId);

        DiseaseUpdatedEvent event = new DiseaseUpdatedEvent(memberId, now(), diseaseId, name, description, startDate);
        member.apply(event);

        memberRepository.save(member);
        eventSender.send(event);
    }
}
