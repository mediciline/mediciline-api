package org.tmcode.mediciline.application.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tmcode.cqrs.CommandHandler;
import org.tmcode.mediciline.domain.EventSender;
import org.tmcode.mediciline.domain.Member;
import org.tmcode.mediciline.domain.MemberRepository;
import org.tmcode.mediciline.event.DiseaseDeletedEvent;

import java.util.UUID;

import static java.time.LocalDateTime.now;

@Component
class DeleteDiseaseHandler implements CommandHandler<DeleteDiseaseCommand> {

    private final MemberRepository memberRepository;
    private final EventSender eventSender;

    @Autowired
    DeleteDiseaseHandler(MemberRepository memberRepository, EventSender eventSender) {
        this.memberRepository = memberRepository;
        this.eventSender = eventSender;
    }

    @Override
    public void handle(DeleteDiseaseCommand command) {

        UUID memberId = command.getMemberId();
        UUID diseaseId = command.getDiseaseId();

        DiseaseDeletedEvent event = new DiseaseDeletedEvent(memberId, now(), diseaseId);
        Member member = memberRepository.get(memberId);
        member.apply(event);
        memberRepository.save(member);
        eventSender.send(event);
    }
}
