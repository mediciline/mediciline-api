package org.tmcode.mediciline.application.command;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.UUID;

@AllArgsConstructor
@Getter
public class DeleteMemberCommand {

    private final UUID memberId;
}
