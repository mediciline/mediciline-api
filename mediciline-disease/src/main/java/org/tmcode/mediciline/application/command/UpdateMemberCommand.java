package org.tmcode.mediciline.application.command;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDate;
import java.util.UUID;

@AllArgsConstructor
@Getter
public class UpdateMemberCommand {

    private final UUID memberId;
    private final String name;
    private final LocalDate birthday;
}
