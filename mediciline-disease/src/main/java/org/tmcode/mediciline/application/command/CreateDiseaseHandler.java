package org.tmcode.mediciline.application.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tmcode.cqrs.CommandHandler;
import org.tmcode.mediciline.domain.EventSender;
import org.tmcode.mediciline.domain.Member;
import org.tmcode.mediciline.domain.MemberRepository;
import org.tmcode.mediciline.event.DiseaseCreatedEvent;

import java.time.LocalDate;
import java.util.UUID;

import static java.time.LocalDateTime.now;

@Component
class CreateDiseaseHandler implements CommandHandler<CreateDiseaseCommand> {

    private final MemberRepository memberRepository;
    private final EventSender eventSender;

    @Autowired
    CreateDiseaseHandler(MemberRepository memberRepository, EventSender eventSender) {
        this.memberRepository = memberRepository;
        this.eventSender = eventSender;
    }

    @Override
    public void handle(CreateDiseaseCommand command) {

        UUID memberId = command.getMemberId();
        UUID diseaseId = command.getDiseaseId();
        String name = command.getName();
        String description = command.getDescription();
        LocalDate startDate = command.getStartDate();

        DiseaseCreatedEvent event = new DiseaseCreatedEvent(memberId, now(), diseaseId, name, description, startDate);
        Member member = memberRepository.get(memberId);
        member.apply(event);
        memberRepository.save(member);
        eventSender.send(event);
    }
}
