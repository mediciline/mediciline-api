package org.tmcode.mediciline.application.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tmcode.cqrs.CommandHandler;
import org.tmcode.mediciline.domain.EventSender;
import org.tmcode.mediciline.domain.Member;
import org.tmcode.mediciline.domain.MemberRepository;
import org.tmcode.mediciline.event.DiseaseFinishedEvent;

import java.time.LocalDate;
import java.util.UUID;

import static java.time.LocalDateTime.now;

@Component
class FinishDiseaseHandler implements CommandHandler<FinishDiseaseCommand> {

    private final MemberRepository memberRepository;
    private final EventSender eventSender;

    @Autowired
    FinishDiseaseHandler(MemberRepository memberRepository, EventSender eventSender) {
        this.memberRepository = memberRepository;
        this.eventSender = eventSender;
    }

    @Override
    public void handle(FinishDiseaseCommand command) {

        UUID memberId = command.getMemberId();

        Member member = memberRepository.get(memberId);

        DiseaseFinishedEvent event = new DiseaseFinishedEvent(memberId, now(), command.getDiseaseId(), LocalDate.now());
        member.apply(event);

        memberRepository.save(member);
        eventSender.send(event);
    }
}
