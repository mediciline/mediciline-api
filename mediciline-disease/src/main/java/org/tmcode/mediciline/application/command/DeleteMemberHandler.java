package org.tmcode.mediciline.application.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tmcode.cqrs.CommandHandler;
import org.tmcode.mediciline.domain.EventSender;
import org.tmcode.mediciline.domain.Member;
import org.tmcode.mediciline.event.MemberDeletedEvent;
import org.tmcode.mediciline.domain.MemberRepository;

import java.util.UUID;

import static java.time.LocalDateTime.now;

@Component
class DeleteMemberHandler implements CommandHandler<DeleteMemberCommand> {

    private final MemberRepository memberRepository;
    private final EventSender eventSender;

    @Autowired
    DeleteMemberHandler(MemberRepository memberRepository, EventSender eventSender) {
        this.memberRepository = memberRepository;
        this.eventSender = eventSender;
    }

    @Override
    public void handle(DeleteMemberCommand command) {

        UUID memberId = command.getMemberId();

        Member member = memberRepository.get(memberId);

        MemberDeletedEvent event = new MemberDeletedEvent(memberId, now());
        member.apply(event);

        memberRepository.save(member);
        eventSender.send(event);
    }
}
