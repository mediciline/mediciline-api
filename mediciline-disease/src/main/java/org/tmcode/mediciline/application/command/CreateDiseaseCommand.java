package org.tmcode.mediciline.application.command;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDate;
import java.util.UUID;

@AllArgsConstructor
@Getter
public class CreateDiseaseCommand {

    private final UUID diseaseId;
    private final UUID memberId;
    private final String name;
    private final String description;
    private final LocalDate startDate;
}
