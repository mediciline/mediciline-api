package org.tmcode.mediciline.application.command;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.UUID;

@AllArgsConstructor
@Getter
public class FinishDiseaseCommand {

    private final UUID memberId;
    private final UUID diseaseId;
}
