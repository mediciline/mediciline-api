package org.tmcode.mediciline.domain;

import com.google.common.collect.ImmutableMap;
import org.tmcode.mediciline.event.DiseaseCreatedEvent;
import org.tmcode.mediciline.event.DiseaseDeletedEvent;
import org.tmcode.mediciline.event.DiseaseFinishedEvent;
import org.tmcode.mediciline.event.DiseaseUpdatedEvent;
import org.tmcode.mediciline.event.MemberCreatedEvent;
import org.tmcode.mediciline.event.MemberDeletedEvent;
import org.tmcode.mediciline.event.MemberUpdatedEvent;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.IntStream;

import static com.google.common.collect.Lists.newArrayList;
import static org.tmcode.mediciline.utills.AssertUtils.isTrue;
import static org.tmcode.mediciline.utills.AssertUtils.notNull;

public class Member {

    private UUID id;

    private String name;
    private LocalDate birthday;
    private boolean isAvailable;
    private List<Disease> diseases = newArrayList();

    private Map<Long, Object> events = new ConcurrentHashMap<>();;
    private AtomicLong version = new AtomicLong(0);

    public Member(Object event) {
        apply(event);
    }

    public void apply(Object event) {

        if (event instanceof MemberCreatedEvent) {
            applyMemberCreatedEvent((MemberCreatedEvent) event);
        }

        isTrue(isAvailable, aBoolean -> aBoolean, CanNotModifyDeletedMemberException::new);

        if (event instanceof MemberUpdatedEvent) {
            applyMemberUpdatedEvent((MemberUpdatedEvent) event);
        }
        if (event instanceof MemberDeletedEvent) {
            applyMemberDeletedEvent((MemberDeletedEvent) event);
        }
        if (event instanceof DiseaseCreatedEvent) {
            applyDiseaseCreatedEvent((DiseaseCreatedEvent) event);
        }
        if (event instanceof DiseaseDeletedEvent) {
            applyDiseaseDeletedEvent((DiseaseDeletedEvent) event);
        }
        if (event instanceof DiseaseUpdatedEvent) {
            applyDiseaseUpdatedEvent((DiseaseUpdatedEvent) event);
        }
        if (event instanceof DiseaseFinishedEvent) {
            applyDiseaseFinishedEvent((DiseaseFinishedEvent) event);
        }

        long version = this.version.incrementAndGet();
        events.put(version, event);
    }

    private void applyMemberCreatedEvent(MemberCreatedEvent event) {
        notNull(event.getMemberId(), () -> new NullValueException("name"));
        notNull(event.getName(), () -> new NullValueException("name"));
        notNull(event.getBirthday(), () -> new NullValueException("birthday"));

        this.id = event.getMemberId();
        this.name = event.getName();
        this.birthday = event.getBirthday();
        this.isAvailable = true;
    }

    private void applyMemberUpdatedEvent(MemberUpdatedEvent event) {
        notNull(event.getMemberId(), () -> new NullValueException("aggregateId"));
        notNull(event.getName(), () -> new NullValueException("name"));
        notNull(event.getBirthday(), () -> new NullValueException("birthday"));
        isTrue(this.id, id -> id.equals(event.getMemberId()), InvalidRootEntityException::new);

        this.name = event.getName();
        this.birthday = event.getBirthday();
    }

    private void applyMemberDeletedEvent(MemberDeletedEvent event) {
        notNull(event.getMemberId(), () -> new NullValueException("aggregateId"));
        isTrue(this.id, id -> id.equals(event.getMemberId()), InvalidRootEntityException::new);
        this.isAvailable = false;
    }

    private void applyDiseaseCreatedEvent(DiseaseCreatedEvent event) {
        notNull(event.getMemberId(), () -> new NullValueException("aggregateId"));
        isTrue(this.id, id -> id.equals(event.getMemberId()), InvalidRootEntityException::new);

        diseases.add(new Disease(event));
    }

    private void applyDiseaseDeletedEvent(DiseaseDeletedEvent event) {
        notNull(event.getMemberId(), () -> new NullValueException("aggregateId"));
        notNull(event.getDiseaseId(), () -> new NullValueException("diseaseId"));

        int index = getDiseaseIndex(event.getDiseaseId());

        diseases.remove(index);
    }

    private void applyDiseaseUpdatedEvent(DiseaseUpdatedEvent event) {
        notNull(event.getMemberId(), () -> new NullValueException("aggregateId"));
        notNull(event.getDiseaseId(), () -> new NullValueException("diseaseId"));
        notNull(event.getName(), () -> new NullValueException("name"));
        notNull(event.getDescription(), () -> new NullValueException("description"));
        notNull(event.getStartDate(), () -> new NullValueException("startDate"));

        int index = getDiseaseIndex(event.getDiseaseId());
        Disease disease = diseases.get(index);
        disease.update(event);
    }

    private void applyDiseaseFinishedEvent(DiseaseFinishedEvent event) {
        notNull(event.getMemberId(), () -> new NullValueException("aggregateId"));
        notNull(event.getDiseaseId(), () -> new NullValueException("diseaseId"));
        int index = getDiseaseIndex(event.getDiseaseId());
        Disease disease = diseases.get(index);
        disease.finish(event);
    }

    private int getDiseaseIndex(UUID diseaseId) {
        return IntStream.range(0, diseases.size())
                .filter(i -> diseaseId.equals(diseases.get(i).getId()))
                .findAny().orElseThrow(NotFoundException::new);
    }

    public Map<Long, Object> listEvents() {
        return ImmutableMap.copyOf(events);
    }

    public UUID getId() {
        return id;
    }
}
