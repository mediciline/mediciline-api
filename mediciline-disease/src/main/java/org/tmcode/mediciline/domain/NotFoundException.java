package org.tmcode.mediciline.domain;

public class NotFoundException extends RuntimeException {
    public NotFoundException() {
        super("NotFound");
    }
}
