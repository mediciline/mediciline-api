package org.tmcode.mediciline.domain;

public class CanNotModifyDeletedMemberException extends RuntimeException {
    public CanNotModifyDeletedMemberException() {
        super("CanNotModifyDeletedMember");
    }
}
