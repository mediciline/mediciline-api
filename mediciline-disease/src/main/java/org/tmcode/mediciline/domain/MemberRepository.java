package org.tmcode.mediciline.domain;

import java.util.UUID;

public interface MemberRepository {
    Member get(UUID drugId);

    void save(Member member);
}
