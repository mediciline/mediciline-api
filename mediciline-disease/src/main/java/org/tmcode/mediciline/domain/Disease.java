package org.tmcode.mediciline.domain;

import org.tmcode.mediciline.event.DiseaseCreatedEvent;
import org.tmcode.mediciline.event.DiseaseFinishedEvent;
import org.tmcode.mediciline.event.DiseaseUpdatedEvent;

import java.time.LocalDate;
import java.util.UUID;

import static java.time.LocalDate.now;
import static org.tmcode.mediciline.utills.AssertUtils.isTrue;
import static org.tmcode.mediciline.utills.AssertUtils.notNull;

class Disease {

    private final UUID id;
    private String name;
    private String description;
    private LocalDate startDate;
    private LocalDate endDate;

    Disease(DiseaseCreatedEvent event) {
        notNull(event.getDiseaseId(), () -> new NullValueException("diseaseId"));
        notNull(event.getName(), () -> new NullValueException("name"));
        notNull(event.getDescription(), () -> new NullValueException("description"));
        notNull(event.getStartDate(), () -> new NullValueException("startDate"));
        isTrue(event.getStartDate(), startDate -> startDate.isBefore(now()) || startDate.isEqual(now()), CanNotCreateDiseaseException::new);
        this.id = event.getDiseaseId();
        this.name = event.getName();
        this.description = event.getDescription();
        this.startDate = event.getStartDate();
    }

    void finish(DiseaseFinishedEvent event) {
        isTrue(this.id, id -> id.equals(event.getDiseaseId()), InvalidIdException::new);
        isTrue(this.startDate, startDate -> startDate.isBefore(event.getFinishDate()) || startDate.isEqual(event.getFinishDate()), CanNotEndDiseaseException::new);
        endDate = event.getFinishDate();
    }

    void update(DiseaseUpdatedEvent event) {
        isTrue(this.id.equals(event.getDiseaseId()), InvalidIdException::new);
        isTrue(event.getStartDate().isBefore(event.getCreatedAt().toLocalDate())
                || event.getStartDate().isEqual(event.getCreatedAt().toLocalDate()), CanNotUpdateDiseaseException::new);

        this.name = event.getName();
        this.description = event.getDescription();
        this.startDate = event.getStartDate();
    }

    UUID getId() {
        return id;
    }
}
