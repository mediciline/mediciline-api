package org.tmcode.mediciline.domain;

public class CanNotEndDiseaseException extends RuntimeException {
    public CanNotEndDiseaseException() {
        super("CanNotEndDisease");
    }
}
