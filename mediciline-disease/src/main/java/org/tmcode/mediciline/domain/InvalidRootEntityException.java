package org.tmcode.mediciline.domain;

public class InvalidRootEntityException extends RuntimeException {
    public InvalidRootEntityException() {
        super("InvalidRootEntity");
    }
}
