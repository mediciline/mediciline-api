package org.tmcode.mediciline.domain;

public class CanNotUpdateDiseaseException extends RuntimeException {
    public CanNotUpdateDiseaseException() {
        super("CanNotUpdateDisease");
    }
}
