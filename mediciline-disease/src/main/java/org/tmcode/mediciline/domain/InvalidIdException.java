package org.tmcode.mediciline.domain;

public class InvalidIdException extends RuntimeException {
    public InvalidIdException() {
        super("InvalidId");
    }
}
