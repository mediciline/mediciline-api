package org.tmcode.mediciline.domain;

public class CanNotCreateDiseaseException extends RuntimeException {
    public CanNotCreateDiseaseException() {
        super("CanNotCreateDisease");
    }
}
