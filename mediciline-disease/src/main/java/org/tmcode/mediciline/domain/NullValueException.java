package org.tmcode.mediciline.domain;

public class NullValueException extends RuntimeException {
    public NullValueException(String field) {
        super(String.format("%s.can.not.be.null", field));
    }
}
