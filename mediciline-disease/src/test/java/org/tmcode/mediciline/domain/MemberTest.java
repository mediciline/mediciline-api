package org.tmcode.mediciline.domain;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.tmcode.mediciline.event.DiseaseCreatedEvent;
import org.tmcode.mediciline.event.DiseaseDeletedEvent;
import org.tmcode.mediciline.event.DiseaseFinishedEvent;
import org.tmcode.mediciline.event.DiseaseUpdatedEvent;
import org.tmcode.mediciline.event.MemberCreatedEvent;
import org.tmcode.mediciline.event.MemberDeletedEvent;
import org.tmcode.mediciline.event.MemberUpdatedEvent;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;
import java.util.stream.Stream;

import static java.time.LocalDateTime.now;
import static java.util.UUID.randomUUID;
import static org.assertj.core.api.Assertions.assertThat;

class MemberTest {

    @Test
    public void shouldCreateMember() {

    	// given
        UUID aggregateId = randomUUID();
        String name = "member name";
        LocalDate birthday = LocalDate.of(1984, 8, 9);
        MemberCreatedEvent event = new MemberCreatedEvent(aggregateId, now(), name, birthday);

        // when
        Member member = new Member(event);

        // then
        assertThat(member).hasFieldOrPropertyWithValue("id", aggregateId);
        assertThat(member).hasFieldOrPropertyWithValue("name", name);
        assertThat(member).hasFieldOrPropertyWithValue("birthday", birthday);
        assertThat(member).hasFieldOrPropertyWithValue("isAvailable", true);
    }

    @Test
    public void shouldApplyMemberUpdatedEvent() {

        // given
        UUID aggregateId = randomUUID();
        Member member = createMember(aggregateId);
        String newName = "new name";
        LocalDate newBirthday = LocalDate.of(1955, 1, 1);
        MemberUpdatedEvent event = new MemberUpdatedEvent(aggregateId, now(), newName, newBirthday);

        // when
        member.apply(event);

        // then
        assertThat(member).hasFieldOrPropertyWithValue("id", aggregateId);
        assertThat(member).hasFieldOrPropertyWithValue("name", newName);
        assertThat(member).hasFieldOrPropertyWithValue("birthday", newBirthday);
    }

    @Test
    public void shouldApplyMemberDeletedEvent() {

        // given
        UUID aggregateId = randomUUID();
        Member member = createMember(aggregateId);
        MemberDeletedEvent event = new MemberDeletedEvent(aggregateId, now());

        // when
        member.apply(event);

        // then
        assertThat(member).hasFieldOrPropertyWithValue("id", aggregateId);
        assertThat(member).hasFieldOrPropertyWithValue("isAvailable", false);
    }

    @Test
    public void shouldThrowExceptionWhenApplyEventOnNotAvailableMember() {

        // given
        UUID aggregateId = randomUUID();
        Member member = createMember(aggregateId);
        MemberDeletedEvent event = new MemberDeletedEvent(aggregateId, now());
        member.apply(event);

        //when
        Assertions.assertThatThrownBy(() -> member.apply(new MemberUpdatedEvent(aggregateId, now(), "ds", LocalDate.now())))

        //then
        .isInstanceOf(CanNotModifyDeletedMemberException.class);
    }

    @Test
    public void shouldApplyDiseaseCreatedEvent() {

        // given
        UUID aggregateId = randomUUID();
        Member member = createMember(aggregateId);
        UUID diseaseId = randomUUID();
        String diseaseName = "disease name";
        String diseaseDescription = "disease description";
        LocalDate startDate = LocalDate.of(2020, 1, 1);
        DiseaseCreatedEvent event = new DiseaseCreatedEvent(aggregateId, now(), diseaseId, diseaseName, diseaseDescription, startDate);

        // when
        member.apply(event);

        // then
        assertThat(member).hasFieldOrPropertyWithValue("id", aggregateId);
        assertThat(member).extracting("diseases").asList().hasSize(1);
        assertThat(member).extracting("diseases").asList().extracting("id").contains(diseaseId);
        assertThat(member).extracting("diseases").asList().extracting("name").contains(diseaseName);
        assertThat(member).extracting("diseases").asList().extracting("description").contains(diseaseDescription);
        assertThat(member).extracting("diseases").asList().extracting("description").contains(diseaseDescription);
        assertThat(member).extracting("diseases").asList().extracting("startDate").contains(startDate);
        assertThat(member).extracting("diseases").asList().extracting("endDate").containsNull();
    }

    @Test
    public void shouldApplyDiseaseDeletedEvent() {

        // given
        UUID aggregateId = randomUUID();
        UUID diseaseId = randomUUID();
        Member member = createMemberWithDisease(aggregateId, diseaseId);
        DiseaseDeletedEvent event = new DiseaseDeletedEvent(aggregateId, now(), diseaseId);
        assertThat(member).extracting("diseases").asList().hasSize(1);

        // when
        member.apply(event);

        // then
        assertThat(member).hasFieldOrPropertyWithValue("id", aggregateId);
        assertThat(member).extracting("diseases").asList().isEmpty();
    }

    @Test
    public void shouldApplyDiseaseUpdatedEvent() {

        // given
        LocalDateTime now = now();

        UUID aggregateId = randomUUID();
        UUID diseaseId = randomUUID();
        Member member = createMemberWithDisease(aggregateId, diseaseId);
        String diseaseName = "new disease name";
        String diseaseDescription = "new disease description";
        LocalDate startDate = now.minusDays(1).toLocalDate();

        DiseaseUpdatedEvent event = new DiseaseUpdatedEvent(aggregateId, now, diseaseId, diseaseName, diseaseDescription, startDate);

        // when
        member.apply(event);

        // then
        assertThat(member).hasFieldOrPropertyWithValue("id", aggregateId);
        assertThat(member).extracting("diseases").asList().hasSize(1);
        assertThat(member).extracting("diseases").asList().extracting("id").contains(diseaseId);
        assertThat(member).extracting("diseases").asList().extracting("name").contains(diseaseName);
        assertThat(member).extracting("diseases").asList().extracting("description").contains(diseaseDescription);
        assertThat(member).extracting("diseases").asList().extracting("description").contains(diseaseDescription);
        assertThat(member).extracting("diseases").asList().extracting("startDate").contains(startDate);
        assertThat(member).extracting("diseases").asList().extracting("endDate").containsNull();
    }

    @ParameterizedTest
    @MethodSource
    public void shouldApplyDiseaseFinishedEvent(LocalDate finishDate) {

        // given
        UUID aggregateId = randomUUID();
        UUID diseaseId = randomUUID();
        Member member = createMemberWithDisease(aggregateId, diseaseId);;

        DiseaseFinishedEvent event = new DiseaseFinishedEvent(aggregateId, now(), diseaseId, finishDate);

        // when
        member.apply(event);

        // then
        assertThat(member).hasFieldOrPropertyWithValue("id", aggregateId);
        assertThat(member).extracting("diseases").asList().hasSize(1);
        assertThat(member).extracting("diseases").asList().extracting("id").contains(diseaseId);
        assertThat(member).extracting("diseases").asList().extracting("endDate").contains(finishDate);
    }

    private static Stream<Arguments> shouldApplyDiseaseFinishedEvent() {
        return Stream.of(
                Arguments.of(LocalDate.of(2050, 1, 1)),
                Arguments.of(LocalDate.of(2020, 1, 1))
        );
    }

    @Test
    public void shouldThrowExceptionWhenEndDateIsBeforeStart() {

        // given
        UUID aggregateId = randomUUID();
        UUID diseaseId = randomUUID();
        Member member = createMemberWithDisease(aggregateId, diseaseId);
        LocalDate finishDate = LocalDate.of(1990, 1, 1);
        DiseaseFinishedEvent event = new DiseaseFinishedEvent(aggregateId, now(), diseaseId, finishDate);

        //when
        Assertions.assertThatThrownBy(() -> member.apply(event))

        //then
        .isInstanceOf(CanNotEndDiseaseException.class);
    }

    @Test
    public void shouldThrowExceptionWhenNotFoundDisease() {

        // given
        UUID aggregateId = randomUUID();
        UUID diseaseId = randomUUID();
        Member member = createMember(aggregateId);
        LocalDate finishDate = LocalDate.of(1990, 1, 1);
        DiseaseFinishedEvent event = new DiseaseFinishedEvent(aggregateId, now(), diseaseId, finishDate);

        //when
        Assertions.assertThatThrownBy(() -> member.apply(event))

        //then
        .isInstanceOf(NotFoundException.class);
    }


    private Member createMember(UUID aggregateId) {
        String name = "member name";
        LocalDate birthday = LocalDate.of(1984, 8, 9);
        MemberCreatedEvent event = new MemberCreatedEvent(aggregateId, now(), name, birthday);
        return new Member(event);
    }

    private Member createMemberWithDisease(UUID aggregateId, UUID diseaseId) {
        Member member = createMember(aggregateId);
        String diseaseName = "disease name";
        String diseaseDescription = "disease description";
        LocalDate startDate = LocalDate.of(2020, 1, 1);
        DiseaseCreatedEvent event = new DiseaseCreatedEvent(aggregateId, now(), diseaseId, diseaseName, diseaseDescription, startDate);
        member.apply(event);
        return member;
    }

}
