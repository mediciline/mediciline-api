package org.tmcode.mediciline.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CreateDiseaseDto {

    private String name;
    private String description;
    private LocalDate startDate;
}
