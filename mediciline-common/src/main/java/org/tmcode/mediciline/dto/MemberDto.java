package org.tmcode.mediciline.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class MemberDto {

    private UUID id;
    private String name;
    private String birthday;
}
