package org.tmcode.mediciline.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class DiseaseDto {

    private UUID id;
    private UUID memberId;
    private String name;
    private String description;
    private String startDate;
    private String endDate;
}
