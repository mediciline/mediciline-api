package org.tmcode.mediciline.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CreateDrugDto {

    private UUID dictionaryDrugId;
    private String expirationDate;
}
