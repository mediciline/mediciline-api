package org.tmcode.mediciline.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class DrugDto {

    private UUID id;
    private UUID dictionaryDrugId;
    private String expirationDate;
    private boolean expired;

}
