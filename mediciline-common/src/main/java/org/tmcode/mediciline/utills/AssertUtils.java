package org.tmcode.mediciline.utills;

import java.util.function.Predicate;
import java.util.function.Supplier;

public class AssertUtils {

    public static void isTrue(boolean condition, Supplier<RuntimeException> runtimeExceptionSupplier) {

        if (!condition) {
            throw runtimeExceptionSupplier.get();
        }
    }

    public static <T> void isTrue(T input, Predicate<T> predicate, Supplier<RuntimeException> runtimeExceptionSupplier) {

        if (!predicate.test(input)) {
            throw runtimeExceptionSupplier.get();
        }
    }

    public static <T> void isFalse(T input, Predicate<T> predicate, Supplier<RuntimeException> runtimeExceptionSupplier) {

        isTrue(input, predicate.negate(), runtimeExceptionSupplier);
    }

    public static <T> void notNull(T input, Supplier<RuntimeException> runtimeExceptionSupplier) {

        if (input == null) {
            throw runtimeExceptionSupplier.get();
        }
    }
}
