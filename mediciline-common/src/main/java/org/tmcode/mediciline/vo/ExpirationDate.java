package org.tmcode.mediciline.vo;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.apache.commons.lang3.Validate.isTrue;
import static org.apache.commons.validator.GenericValidator.isDate;

@NoArgsConstructor
@EqualsAndHashCode
public class ExpirationDate {

    private final static String PATTERN = "yyyy-MM";
    private final static DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern(PATTERN);

    private LocalDate date;

    /**
     *
     * @param date format yyyy-MM
     */
    public ExpirationDate(String date) {
        isTrue(isDate(date, PATTERN, true), "invalid.date.format");
        LocalDate parsedDate = LocalDate.parse(date + "-01");
        this.date = parsedDate.withDayOfMonth(parsedDate.lengthOfMonth());
    }

    public String obtainValue() {
        return date.format(FORMATTER);
    }

    public boolean isBefore(LocalDate now) {
        return date.isBefore(now);
    }
    public boolean isAfter(LocalDate now) {
        return date.isAfter(now);
    }
}
