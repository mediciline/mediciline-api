package org.tmcode.mediciline.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.tmcode.mediciline.vo.ExpirationDate;

import java.time.LocalDateTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class DrugCreatedEvent {

    private UUID drugId;
    private LocalDateTime createdAt;
    private UUID dictionaryDrugId;
    private ExpirationDate expirationDate;
}
