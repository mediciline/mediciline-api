package org.tmcode.mediciline.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class DrugNotExpiredEvent {

    private UUID drugId;
    private LocalDateTime createdAt;
}
