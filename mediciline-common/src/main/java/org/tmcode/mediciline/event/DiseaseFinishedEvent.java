package org.tmcode.mediciline.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class DiseaseFinishedEvent {

    private UUID memberId;
    private LocalDateTime createdAt;
    private UUID diseaseId;
    private LocalDate finishDate;
}
