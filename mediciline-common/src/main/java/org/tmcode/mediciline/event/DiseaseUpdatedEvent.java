package org.tmcode.mediciline.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class DiseaseUpdatedEvent {

    private UUID memberId;
    private LocalDateTime createdAt;
    private UUID diseaseId;
    private String name;
    private String description;
    private LocalDate startDate;
}
