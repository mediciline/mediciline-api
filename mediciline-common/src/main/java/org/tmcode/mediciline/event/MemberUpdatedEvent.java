package org.tmcode.mediciline.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class MemberUpdatedEvent {

    private UUID memberId;
    private LocalDateTime createdAt;
    private String name;
    private LocalDate birthday;
}
